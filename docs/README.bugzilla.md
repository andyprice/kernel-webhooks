# Bugzilla Webhook

## Purpose

This webhook verifies that a Merge Request's description and commits all reference valid bugzillas.

## Notifications

This webhook will report the Bugzilla status of a Merge Request in a comment.  Any updates to that status will be delivered via edits to that same comment.

##  Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.bugzilla \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --note request-bz-evaluation

The --note parameter "fakes" a comment being added to a merge request, as if
you added it through the gitlab webui yourself (but the comment itself isn't
actually added there). The example above shows the predefined comment keyword we
have to trigger bugzilla re-evaluation.

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

In addition, this webhook requires two environment variables to be set:

- BUGZILLA_API_KEY: a [bugzilla API key](https://bugzilla.redhat.com/userprefs.cgi?tab=apikey)
- COMMIT_POLICY_URL: the full URL of the dist-git commit policy file
