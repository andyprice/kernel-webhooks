# Sprinter

## Purpose

This is a simple webhook that listens to closed and reopened issues and tags
them with a standarized label for planning purposes.

The tag is composed of the year and the week number when the issue is closed:
`sprint::{year}-week-{week_number}`.

Running the service requires the following parameters:

    python3 -m webhook.sprinter \
        --rabbitmq-host RABBITMQ_HOST \
        --rabbitmq-port RABBITMQ_PORT \
        --rabbitmq-user RABBITMQ_USER \
        --rabbitmq-password RABBITMQ_PASSWORD \
        --rabbitmq-exchange WEBHOOK_RECEIVER_EXCHANGE \
        --rabbitmq-routing-key SPRINTER_ROUTING_KEYS


## Configuration

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

    | Environment variable | Description                           |
    |----------------------|---------------------------------------|
    | `LABELS_NAMESPACE`   | Gitlab group where labels are created |
