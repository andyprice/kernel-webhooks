"""Tests for the description library."""
from unittest import TestCase
from unittest import mock

from tests.fakes_bz import FakeBZ
from webhook import description


class TestDescription(TestCase):
    """Tests for the Description dataclass."""

    text = ('Hello\n'
            'THINGS THAT SHOULD MATCH:\n'
            'Bugzilla: https://bugzilla.redhat.com/123456  \n'                         # BZ 123456
            'Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=637382\n'            # BZ 637382
            'CVE: CVE-2021-61677\n'                                               # CVE-2021-61677
            'CVE: CVE-1992-15616\n'                                               # CVE-1992-15616
            'Depends: https://bugzilla.redhat.com/262727  \n'                         # Dep 262727
            'Signed-off-by: User Name <user@example.com> \n'  # DCO 'User Name', 'user@example.com'
            'Depends: !737\n'                                                          # Dep !737
            'Depends: https://gitlab.com/group/project/-/merge_requests/267\n'         # Dep !267
            'Signed-off-by: Artist <artist@example.com>\n'    # DCO 'Artist', 'artist@example.com'
            'hey\nDepends: http://gitlab.com/group1/project/-/merge_requests/321  \n'  # Dep !321
            'THINGS THAT SHOULD NOT MATCH:\n'
            'Bugzilla: 34567\nBugzilla: BZ-456789\n'
            'Signed-off-by: example <example@example.com> 😎\n'
            '    Bugzilla: https://bugzilla.redhat.com/23456\n'
            '    Signed-off-by: Person <person@example.com>\n'
            )

    def test_Description_empty(self):
        """Returns as False"""
        test_desc = description.Description(text='')
        self.assertIs(bool(test_desc), False)
        self.assertEqual(test_desc.bugzilla, set())
        self.assertEqual(test_desc.cve, set())
        self.assertEqual(test_desc.depends, set())
        self.assertEqual(test_desc.depends_mrs, set())
        self.assertIs(test_desc.marked_internal, False)

    def test_Description_equal(self):
        """Evaluates as equal when the text, namespace, and _depends match."""
        namespace = 'space/project'
        desc1 = description.Description(text=self.text, namespace=namespace)
        desc1.add_to_depends([101, 202])
        desc2 = description.Description(text=self.text, namespace=namespace)
        self.assertNotEqual(desc1, desc2)
        desc2.add_to_depends([101, 202])
        self.assertEqual(desc1, desc2)

    def test_Description_bugzilla(self):
        """Parses the Bugzilla: tags."""
        test_desc = description.Description(text=self.text)
        self.assertEqual(test_desc.bugzilla, {123456, 637382})

    def test_Description_cve(self):
        """Parses the CVE: tags."""
        test_desc = description.Description(text=self.text)
        self.assertEqual(test_desc.cve, {'CVE-2021-61677', 'CVE-1992-15616'})

    def test_Description_depends(self):
        """Parses the Depends: tags for BZ IDs."""
        test_desc = description.Description(text=self.text)
        self.assertEqual(test_desc.depends, {262727})

    def test_Description_depends_with_depends(self):
        """Parses the Depends: tags for BZ IDs and appends _depends."""
        test_desc = description.Description(text=self.text)
        test_desc.add_to_depends([123, 456])
        self.assertEqual(test_desc.depends, {123, 262727, 456})

    def test_Description_depends_mrs_no_namespace(self):
        """Parses the Depends: tags for MR IDs."""
        test_desc = description.Description(text=self.text)
        self.assertEqual(test_desc.depends_mrs, {737})

    @mock.patch('webhook.description.fetch_bugs', mock.Mock(return_value=[]))
    def test_Description_depends_mrs_with_namespace_no_fetch_bugs(self):
        """Parses the Depends: tags for MR IDs."""
        namespace = 'group1/project'
        test_desc = description.Description(text=self.text, namespace=namespace)
        self.assertEqual(test_desc.depends_mrs, {321, 737})

    @mock.patch('webhook.description.fetch_bugs')
    def test_Description_depends_mrs_with_namespace(self, mock_fetch_bugs):
        """Parses the Depends: tags for MR IDs and get MR IDs from bugzilla."""
        namespace = 'group/project'
        et_data1 = {'ext_bz_bug_id': 'group1/project/-/merge_requests/555',
                    'type': {'url': 'https://gitlab.com/'}}
        et_data2 = {'ext_bz_bug_id': f'{namespace}/-/merge_requests/999',
                    'type': {'url': 'https://gitlab.com/'}}
        et_data3 = {'ext_bz_bug_id': f'{namespace}/-/merge_requests/526',
                    'type': {'url': 'https://gitlab.com/'}}
        fake_bug = FakeBZ(id=262727, external_bugs=[et_data1, et_data2, et_data3])
        mock_fetch_bugs.return_value = [fake_bug]
        test_desc = description.Description(text=self.text, namespace=namespace)
        self.assertEqual(test_desc.depends_mrs, {267, 526, 737, 999})

    def test_Description_marked_internal(self):
        """Returns True if Bugzilla: INTERNAL is found, otherwise False."""
        test_desc = description.Description(text=self.text)
        self.assertIs(test_desc.marked_internal, False)

        text = self.text + '\nBugzilla: INTERNAL  \n'
        test_desc = description.Description(text=text)
        self.assertIs(test_desc.marked_internal, True)

    def test_Description_signoff(self):
        """Parses the Signed-off-by: tags for name/email tuples."""
        test_desc = description.Description(text=self.text)
        self.assertEqual(test_desc.signoff, {('User Name', 'user@example.com'),
                                             ('Artist', 'artist@example.com')})
