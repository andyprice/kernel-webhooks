"""Fake bugzilla objects."""
from unittest import mock


class FakeBZ:
    """A fake bugzilla.bug.Bug object."""

    def __init__(self, id, **kwargs):
        """Set up the basic values."""
        self.id = self.bug_id = int(id)
        # This list of attributes should match defs.BUG_FIELDS.
        self.alias = []
        self.cf_internal_target_release = '---'
        self.cf_verified = []
        # Unlike ITR, if ZTR is not set on a BZ then this object attribute will not exist 🤷.
        # self.cf_zstream_target_release = '---'
        self.component = 'kernel'
        self.depends_on = []
        self.external_bugs = []
        self.flags = []
        self.priority = 'unspecified'
        self.product = 'Red Hat Enterprise Linux 9'
        self.resolution = ''
        self.status = 'NEW'
        self.summary = ''

        # Set values from kwargs.
        for item in kwargs:
            if item != 'cf_zstream_target_release' and not hasattr(self, item):
                raise ValueError(f"'{item}' is not an expected attribute for FakeBZ.")
            setattr(self, item, kwargs[item])

        # The cf_ attributes do not exist on Security Response (CVE) bugs:
        if self.product == 'Security Response':
            for attrib in ['cf_internal_target_release', 'cf_verified']:
                if hasattr(self, attrib):
                    delattr(self, attrib)

        # Add the 'bugzilla' attribute since much of the code expects it to be set.
        self.bugzilla = mock.Mock()

    def __repr__(self):
        """Return a useful identifier."""
        return f'<FakeBZ #{self.id}>'


# Some dummy BZs follow

# A y-stream CVE clone in POST, not Verified:Tested.
BZ1234567 = FakeBZ(id=1234567,
                   cf_internal_target_release='9.1.0',
                   status='POST',
                   summary='CVE-1235-13516 kernel: a cve for the kernel i guess')

# A y-stream BZ in MODIFIED with Verified:Tested and external tracker set.
BZ2323232_EB1 = {'ext_bz_bug_id': 'group/project/-/merge_requests/777',
                 'type': {'url': 'https://gitlab.com/'}
                 }

BZ2323232 = FakeBZ(id=2323232,
                   cf_internal_target_release='9.1.0',
                   cf_verified=['Tested'],
                   external_bugs=[BZ2323232_EB1],
                   status='MODIFIED',
                   summary='some other bug')

# A y-stream BZ in POST.
BZ7777777 = FakeBZ(id=7777777,
                   cf_internal_target_release='9.1',
                   status='POST',
                   summary='kernel: another bug')

# A z-stream kernel-rt CVE BZ in MODIFIED, Verified:Tested set.
BZ2345678 = FakeBZ(id=2345678,
                   alias=[],
                   cf_verified=['Tested'],
                   cf_zstream_target_release='9.0',
                   component='kernel-rt',
                   priority='low',
                   status='MODIFIED',
                   summary='CVE-2022-43210 kernel-rt: a different bug')

# A y-stream BZ in ASSINGED state that has Verified:FailedQA set.
BZ2673283 = FakeBZ(id=2673283,
                   cf_internal_target_release='9.1',
                   cf_verified=['FailedQA'],
                   priority='medium',
                   status='ASSIGNED',
                   summary='kernel: another bug 2.0')

# A top-level CVE tracking bug.
BZ3456789 = FakeBZ(id=3456789,
                   alias=['CVE-1235-13516'],
                   component='vulnerability',
                   priority='high',
                   product='Security Response',
                   status='NEW',
                   summary='CVE-1235-13516 kernel: a bad problem')

# A top-level CVE tracking bug with two CVEs :/.
BZ4567890 = FakeBZ(id=4567890,
                   alias=['MegaFlaw', 'CVE-2022-7549', 'CVE-2022-7550'],
                   component='vulnerability',
                   priority='urgent',
                   product='Security Response',
                   resolution='ERRATA',
                   status='CLOSED',
                   summary='CVE-2022-7549 CVE-2022-7550 kernel: a worse problem')
