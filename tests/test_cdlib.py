"""Webhook interaction tests."""
from unittest import TestCase
from unittest import mock

from webhook import cdlib
from webhook import defs
from webhook import owners


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCDLib(TestCase):

    def test_extract_files(self):
        """Check function returns expected file list."""

        diff_a = {'new_path': 'net/dev/core.c'}
        diff_b = {'new_path': 'redhat/Makefile'}

        commit = mock.Mock()
        commit.diff.return_value = [diff_a, diff_b]
        self.assertEqual(cdlib.extract_files(commit),
                         ['net/dev/core.c', 'redhat/Makefile'])

    def test_mr_get_diff_ids(self):
        """Test the webhook.cdlib.mr_get_diff_ids function."""
        mrequest = mock.MagicMock()
        diff1 = mock.MagicMock(id=1234)
        diff2 = mock.MagicMock(id=5678)
        mrequest.diffs.list.return_value = [diff1, diff2]
        diff_ids = cdlib.mr_get_diff_ids(mrequest)
        self.assertEqual(diff_ids, [1234, 5678])

    def test_mr_get_last_two_diff_ranges(self):
        """Test the webhook.cdlib.mr_get_last_two_diff_ranges function."""
        mrequest = mock.MagicMock()
        diff = mock.MagicMock(id=1234, head_commit_sha='abcd', start_commit_sha='ef01',
                              created_at='2021-02-10T19:13:53.254Z')
        mrequest.diffs.get.return_value = diff
        (latest, old) = cdlib.mr_get_last_two_diff_ranges(mrequest, [1234])
        self.assertEqual(latest['head'], 'abcd')
        self.assertEqual(latest['start'], 'ef01')
        self.assertEqual(latest['created'], '2021-02-10T19:13:53.254Z')

    def test_mr_get_latest_start_sha(self):
        """Test the webhook.cdlib.mr_get_latest_start_sha function."""
        mrequest = mock.MagicMock()
        latest = {'head': '', 'start': 'deadbeef', 'created': ''}
        mrequest.labels = ['Dependencies::abcdef0123']
        start_sha = cdlib.mr_get_latest_start_sha(mrequest, latest['start'])
        self.assertEqual(start_sha, 'abcdef0123')
        mrequest.labels = ['Dependencies::OK']
        start_sha = cdlib.mr_get_latest_start_sha(mrequest, latest['start'])
        self.assertEqual(start_sha, 'deadbeef')

    def test_is_first_dep(self):
        """Test the webhook.cdlib.is_first_dep function."""
        commit = mock.Mock(id="abcdef0")
        dep_sha = 'abcdef0'
        retval = cdlib.is_first_dep(commit, dep_sha)
        self.assertTrue(retval)
        dep_sha = 'deadbeef'
        retval = cdlib.is_first_dep(commit, dep_sha)
        self.assertFalse(retval)

    def test_get_dependencies_data(self):
        """Test the webhook.cdlib.get_dependencies_data function."""
        mrequest = mock.MagicMock()
        mrequest.diff_refs['start_sha'] = 'abbadabba'
        mrequest.labels = ['Dependencies::deafabba']
        (has_deps, dep_sha) = cdlib.get_dependencies_data(mrequest)
        self.assertTrue(has_deps)
        self.assertEqual(dep_sha, 'deafabba')
        mrequest.labels = ['Dependencies::OK']
        (has_deps, dep_sha) = cdlib.get_dependencies_data(mrequest)
        self.assertFalse(has_deps)

    def test_mr_get_old_start_sha(self):
        """Test the webhook.cdlib.mr_get_old_start_sha function."""
        mrequest = mock.MagicMock()
        # We have a removed Dependencies::<hash> label to consider
        rlevent = mock.MagicMock(id=8675309,
                                 label={'id': 1234, 'name': 'Dependencies::abcd'},
                                 created_at='2021-02-10T19:15:09.451Z', action='remove')
        old = {'head': '', 'start': 'eeeeeeee', 'created': '2021-02-10T19:14:09.451Z'}
        latest = {'head': '', 'start': 'ffffffff', 'created': '2021-02-14T19:14:09.451Z'}
        cc_ts = '2021-02-10T19:14:09.451Z'
        mrequest.resourcelabelevents.list.return_value = [rlevent]
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'abcd')
        # We have a stale removed Dependencies::<hash> label to consider
        cc_ts = '2021-02-09T19:14:09.451Z'
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'eeeeeeee')
        # We have an event newer than latest
        latest = {'head': '', 'start': 'ffffffff', 'created': '2021-02-01T19:14:09.451Z'}
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'eeeeeeee')
        # We have no evtid.label
        rlevent = mock.MagicMock(id=8675309, label=False)
        mrequest.resourcelabelevents.list.return_value = [rlevent]
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'eeeeeeee')
        # We have no Dependencies:: labels to consider
        rlevent = mock.MagicMock(id=8675309,
                                 label={'id': 1234, 'name': 'ILikePie::OK'},
                                 created_at='2021-02-12T19:14:09.451Z', action='remove')
        old = {'head': '', 'start': 'ffffffff', 'created': '2021-02-10T19:14:09.451Z'}
        latest = {'head': '', 'start': 'ffffffff', 'created': '2021-02-14T19:14:09.451Z'}
        mrequest.resourcelabelevents.list.return_value = [rlevent]
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'ffffffff')
        # We have a removed Dependencies::OK label to consider
        rlevent = mock.MagicMock(id=8675309,
                                 label={'id': 1234, 'name': 'Dependencies::OK'},
                                 created_at='2021-02-12T19:14:09.451Z', action='remove')
        old = {'head': '', 'start': 'deadbeef', 'created': '2021-02-10T19:14:09.452Z'}
        mrequest.resourcelabelevents.list.return_value = [rlevent]
        start_sha = cdlib.mr_get_old_start_sha(mrequest, old, latest, cc_ts)
        self.assertEqual(start_sha, 'deadbeef')

    @mock.patch('git.Repo')
    def test_get_git_diff(self, mocked_git_repo):
        """Test the webhook.cdlib.get_git_diff function."""
        latest = {'head': 'beef', 'start': 'dead', 'created': '2021-02-12T17:14:29.362Z'}
        old = {'head': 'eeee', 'start': 'ffff', 'created': '2021-02-10T19:14:09.451Z'}
        with self.assertLogs('cki.webhook.cdlib', level='INFO') as logs:
            old_diff = cdlib.get_git_diff(mocked_git_repo, old)
            new_diff = cdlib.get_git_diff(mocked_git_repo, latest)
            self.assertNotEqual(old_diff, None)
            self.assertNotEqual(new_diff, None)
            self.assertIn('Generating diff: ffff..eeee, 2021-02-10T19:14:09.451Z', logs.output[-2])
            self.assertIn('Generating diff: dead..beef, 2021-02-12T17:14:29.362Z', logs.output[-1])

    @mock.patch('webhook.cdlib.get_submitted_diff')
    def test_get_diff_from_mr(self, mocked_sub_diff):
        """Test the webhook.cdlib.get_diff_from_mr function."""
        mrequest = mock.MagicMock()
        diff_ids = [1234, 5678]
        latest = {'head': 'beef', 'start': 'dead', 'created': '2021-02-12T17:14:29.362Z'}
        old = {'head': 'eeee', 'start': 'ffff', 'created': '2021-02-10T19:14:09.451Z'}
        mocked_sub_diff.return_value = ('diff stuff', 'filelist')
        old_diff = cdlib.get_diff_from_mr(mrequest, diff_ids, old)
        new_diff = cdlib.get_diff_from_mr(mrequest, diff_ids, latest)
        self.assertEqual(old_diff, None)
        self.assertEqual(new_diff, None)

    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._find_extra_required_labels')
    @mock.patch('webhook.common._filter_mr_labels')
    @mock.patch('webhook.cdlib.compare_commits')
    @mock.patch('webhook.cdlib.get_diff_from_mr')
    @mock.patch('webhook.cdlib.get_git_diff')
    @mock.patch('webhook.cdlib.mr_get_old_start_sha')
    @mock.patch('webhook.cdlib.mr_get_latest_start_sha')
    @mock.patch('webhook.cdlib.mr_get_last_two_diff_ranges')
    @mock.patch('webhook.cdlib.mr_get_diff_ids')
    @mock.patch('git.Repo')
    @mock.patch('os.path.exists')
    def test_mr_code_changed(self, mocked_exists, mocked_repo, mocked_diff_ids, mocked_ranges,
                             mocked_latest, mocked_old, mocked_diff, mocked_mr_diff,
                             mocked_interdiff, mocked_flabels, mocked_rlabels, mocked_slabels):
        """Test out code that parses revisions of MRs for changes."""
        instance = mock.Mock()
        project = mock.Mock()
        mrequest = mock.MagicMock()
        mrequest.references['full'] = 'mocked'
        mrequest.labels = []
        project.mergerequests.get.return_value = mrequest
        mocked_exists.return_value = True
        mocked_diff_ids.return_value = [1234]
        rhkernel_src = 'mocked'
        latest = {'head': 'beef', 'start': 'dead', 'created': '2021-02-12T17:14:29.362Z'}
        old = {'head': 'eeee', 'start': 'ffff', 'created': '2021-02-10T19:14:09.451Z'}
        (status, rep) = cdlib.mr_code_changed(instance, project, mrequest, rhkernel_src)
        self.assertFalse(status)
        mocked_diff_ids.return_value = [1234, 5678]
        mocked_ranges.return_value = (latest, old)
        mocked_latest.return_value = 'abc123'
        mocked_old.return_value = '123abc'
        mocked_diff.return_value = "foo"
        mocked_mr_diff.return_value = "abc"
        mocked_interdiff.return_value = "something changed"
        mocked_flabels.return_value = ([], [])
        mocked_rlabels.return_value = []
        mocked_slabels.return_value = []
        with self.assertLogs('cki.webhook.cdlib', level='INFO') as logs:
            # git diff and things have changed
            (status, rep) = cdlib.mr_code_changed(instance, project, mrequest, rhkernel_src)
            self.assertIn('Fetching prior diff for MR', logs.output[-2])
            self.assertIn('Code changed after update.', logs.output[-1])
            self.assertTrue(status)

            # MR diff and things have changed
            mocked_latest.return_value = latest['start']
            mocked_old.return_value = old['start']
            (status, rep) = cdlib.mr_code_changed(instance, project, mrequest, rhkernel_src)
            self.assertIn('Fetching prior diff from MR', logs.output[-2])
            self.assertIn('Code changed after update.', logs.output[-1])
            self.assertTrue(status)

            # MR diff and things have not changed
            mocked_interdiff.return_value = ""
            (status, rep) = cdlib.mr_code_changed(instance, project, mrequest, rhkernel_src)
            self.assertIn('No code changes found after update.', logs.output[-1])
            self.assertFalse(status)

            # unable to get any diffs to compare
            mocked_mr_diff.return_value = None
            (status, rep) = cdlib.mr_code_changed(instance, project, mrequest, rhkernel_src)
            self.assertIn('Unable to find one or more diffs, assume code changed', logs.output[-1])
            self.assertTrue(status)

            # head and start sha identical, don't bother fetching diffs
            latest = {'head': 'beef', 'start': 'dead', 'created': '2021-02-12T17:14:29.362Z'}
            old = {'head': 'beef', 'start': 'dead', 'created': '2021-02-12T17:14:29.362Z'}
            mocked_latest.return_value = latest['start']
            mocked_old.return_value = old['start']
            mocked_ranges.return_value = (latest, old)
            (status, rep) = cdlib.mr_code_changed(instance, project, mrequest, rhkernel_src)
            self.assertIn('Current and prior head shas match, no code changes were made.',
                          logs.output[-1])
            self.assertFalse(status)

            # the git repo does not exist
            mocked_exists.return_value = False
            (status, rep) = cdlib.mr_code_changed(instance, project, mrequest, rhkernel_src)
            self.assertIn('No git tree exists at', logs.output[-1])
            self.assertTrue(status)

    @mock.patch('webhook.cdlib.get_diff_from_mr')
    @mock.patch('webhook.cdlib.mr_get_last_two_diff_ranges')
    @mock.patch('webhook.cdlib.mr_get_diff_ids')
    @mock.patch('git.Repo')
    @mock.patch('os.path.exists')
    def test_mr_code_changed2(self, mocked_exists, mocked_repo, mocked_diff_ids, mocked_ranges,
                              mocked_diff):
        """Test MR with dependency, which is updated, compares correct old/latest git range"""
        instance = mock.Mock()
        project = mock.Mock()
        mrequest = mock.MagicMock()
        mrequest.references['full'] = 'mocked'
        dep_hash = 'abcabcabc'
        mrequest.labels = ['Dependencies::' + dep_hash]
        mocked_exists.return_value = True
        rhkernel_src = 'mocked'
        latest = {'head': 'beef2', 'start': 'dead', 'created': '2021-02-12T17:14:29.362Z'}
        old = {'head': 'beef1', 'start': 'dead', 'created': '2021-02-10T19:14:09.451Z'}
        mocked_diff_ids.return_value = [1234, 5678]
        mocked_ranges.return_value = (latest, old)
        mocked_diff.return_value = "foo"

        def mock_compare_commits(a, b):
            return a == b

        mock.patch('webhook.cdlib.compare_commits', new=mock_compare_commits)

        evt = mock.MagicMock()
        evt.id = 1
        evt.label = {}
        evt.label['name'] = mrequest.labels[0]
        evt.action = 'add'
        evt.created_at = '2021-02-10T19:14:10.451Z'

        def mocked_labelevents_list(iterator=True):
            return [evt]

        mrequest.resourcelabelevents.list = mocked_labelevents_list

        with self.assertLogs('cki.webhook.cdlib', level='INFO') as logs:
            def my_get_git_diff(repo, p_rev):
                if p_rev['start'] == dep_hash and p_rev['head'] == old['head']:
                    return mocked_diff.return_value
                elif p_rev['start'] == dep_hash and p_rev['head'] == latest['head']:
                    return mocked_diff.return_value
                else:
                    return '1'

            with mock.patch('webhook.cdlib.get_git_diff', new=my_get_git_diff):
                (status, rep) = cdlib.mr_code_changed(instance, project, mrequest, rhkernel_src)
                self.assertIn('No code changes found after update.', logs.output[-1])
                self.assertFalse(status)

    @mock.patch('webhook.cdlib.mr_get_latest_start_sha')
    @mock.patch('webhook.cdlib.mr_get_diff_ids')
    def test_get_filtered_changed_files(self, get_diff_ids, get_start_sha):
        gl_mergerequest = mock.Mock()
        get_diff_ids.return_value = ['1234', '5678']
        get_start_sha.return_value = 'abcd1234'
        c1 = mock.MagicMock(id="12345678")
        c2 = mock.MagicMock(id="deadbeef")
        c1.diff.return_value = [{'new_path': 'kernel/fork.c'}]
        c2.diff.return_value = [{'new_path': 'include/linux/netdevice.h'}]
        gl_mergerequest.commits.return_value = [c1, c2]
        filelist = cdlib.get_filtered_changed_files(gl_mergerequest)
        self.assertEqual(filelist, ['include/linux/netdevice.h', 'kernel/fork.c'])
        get_start_sha.return_value = 'deadbeef'
        filelist = cdlib.get_filtered_changed_files(gl_mergerequest)
        self.assertEqual(filelist, ['kernel/fork.c'])

    def test_find_first_dependency_commit(self):
        """Test find_first_dependency_commit's functionality."""
        project = mock.Mock()
        project.commits.get.return_value = mock.Mock(parent_ids=["1234"])
        merge_request = mock.Mock(target_branch='main')
        message = ('This BZ has dependencies.\n'
                   'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                   'Depends: https://bugzilla.redhat.com/22334455\n'
                   'Depends: https://bugzilla.redhat.com/33445566\n')
        merge_request.description = message
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       message="1\ncommit 1234567890abcdef1234567890abcdef12345678",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       message="2\ncommit 12345678", parent_ids=["1234"])
        c3 = mock.Mock(id="890a", author_email="jdoe@redhat.com",
                       message="3\nBugzilla: https://bugzilla.redhat.com/33445566",
                       parent_ids=["f00d"])
        merge_request.commits.return_value = [c1, c2, c3]
        output = cdlib.find_first_dependency_commit(project, merge_request)
        self.assertEqual(output, ('890a', True))
        project.commits.get.return_value = mock.Mock(id="beef", parent_ids=["1234", "5678"])
        output = cdlib.find_first_dependency_commit(project, merge_request)
        self.assertEqual(output, ('beef', False))

    def test_get_dependencies_label_scope(self):
        """Test that a Dependencies label gets set properly."""
        scope = cdlib.get_dependencies_label_scope("1234567890abcdef")
        self.assertEqual("1234567890ab", scope)
        scope = cdlib.get_dependencies_label_scope(None)
        self.assertEqual("OK", scope)

    @mock.patch('webhook.common.add_label_to_merge_request')
    def test_set_code_changed_label(self, add_label):
        """Make sure we see the expected call here."""
        mock_inst = mock.Mock()
        mock_proj = mock.Mock()
        mock_mr = mock.Mock()
        mock_mr.labels = []
        mock_proj.mergerequests.get.return_value = mock_mr
        mr_id = 2
        revision = 7
        mock_mr.iid = mr_id
        name = f'{defs.CODE_CHANGED_PREFIX}v{revision}'
        cdlib.set_code_changed_label(mock_inst, mock_proj, mr_id, revision)
        add_label.assert_called_with(mock_inst, mock_proj, mr_id, [name])
        mock_mr.labels = [name]
        add_label.call_count = 0
        cdlib.set_code_changed_label(mock_inst, mock_proj, mr_id, revision)
        add_label.assert_not_called()

    @mock.patch('webhook.cdlib.mr_get_diff_ids')
    def test_get_last_code_changed_timestamp(self, get_diff_ids):
        """Test that we get a TS back from get_last_code_changed_timestamp when we should."""
        mock_mr = mock.Mock()
        rev = 2
        timestamp = '2021-02-10T19:13:53.254Z'
        mock_mr.labels = [f'{defs.CODE_CHANGED_PREFIX}v{rev}']
        get_diff_ids.return_value = ['1234', '5678']

        diff = mock.MagicMock(id=5678, head_commit_sha='abcd', start_commit_sha='ef01',
                              created_at=timestamp)
        mock_mr.diffs.get.return_value = diff

        output = cdlib.get_last_code_changed_timestamp(mock_mr)
        self.assertEqual(output, timestamp)
        get_diff_ids.assert_called_once()

        get_diff_ids.call_count = 0
        mock_mr.labels = []
        output = cdlib.get_last_code_changed_timestamp(mock_mr)
        self.assertEqual(output, None)
        get_diff_ids.assert_not_called()

    @mock.patch('webhook.common.add_plabel_to_merge_request')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.cdlib.find_first_dependency_commit')
    def test_set_dependencies_label(self, first_dep, add_label, add_plabel):
        """Test that Dependencies labels are set correctly."""
        # Should be a group label if scope == OK, otherwise, project label
        instance = mock.Mock()
        project = mock.Mock()
        merge_request = mock.Mock()
        merge_request.iid = 617
        dep_sha = "abcdef0123456789"
        # Has dependency commits
        first_dep.return_value = (dep_sha, True)
        output = cdlib.set_dependencies_label(instance, project, merge_request)
        self.assertEqual(output, f'Dependencies::{dep_sha[:12]}')
        add_plabel.assert_called_with(project, 617, [f'Dependencies::{dep_sha[:12]}'], True)
        # Has no dependencies
        first_dep.return_value = (None, False)
        output = cdlib.set_dependencies_label(instance, project, merge_request)
        self.assertEqual(output, f'Dependencies::{defs.READY_SUFFIX}')
        add_label.assert_called_with(instance, project, 617,
                                     [f'Dependencies::{defs.READY_SUFFIX}'])
        # Has merge commits, but no dependency commits
        first_dep.return_value = (dep_sha, False)
        output = cdlib.set_dependencies_label(instance, project, merge_request)
        self.assertEqual(output, f'Dependencies::{defs.READY_SUFFIX}::{dep_sha[:12]}')
        add_plabel.assert_called_with(project, 617,
                                      [f'Dependencies::{defs.READY_SUFFIX}::{dep_sha[:12]}'],
                                      True)

    CHANGES = {'changes': [{'old_path': 'drivers/net/ethernet/intel/ixgbe/ixgbe.h',
                            'new_path': 'drivers/net/ethernet/intel/ixgbe/ixgbe.h'},
                           {'old_path': 'include/linux/mm.h',
                            'new_path': 'include/linux/mm2.h'},
                           {'old_path': 'include/net/bonding.h',
                            'new_path': 'include/net/bond_xor.h'}]
               }

    @mock.patch('webhook.cdlib.get_filtered_changed_files')
    @mock.patch('webhook.cdlib.set_dependencies_label')
    def test_get_mr_pathlist(self, dep_label, files):
        mock_inst = mock.Mock()
        mock_proj = mock.Mock()
        mock_mr = mock.Mock()
        mock_mr.changes.return_value = {}
        mock_proj.mergerequests.return_value = mock_mr
        mock_mr.labels = ['Dependencies::OK']
        dep_label.return_value = "Dependencies::OK"
        files.return_value = ['include/linux/netdevice.h']
        result = cdlib.get_mr_pathlist(mock_inst, mock_proj, mock_mr)
        self.assertEqual(result, [])
        mock_mr.changes.return_value = self.CHANGES
        result = cdlib.get_mr_pathlist(mock_inst, mock_proj, mock_mr)
        self.assertEqual(sorted(result), ['drivers/net/ethernet/intel/ixgbe/ixgbe.h',
                                          'include/linux/mm.h',
                                          'include/linux/mm2.h',
                                          'include/net/bond_xor.h',
                                          'include/net/bonding.h'])
        mock_mr.labels = ['Dependencies::abcdefg']
        dep_label.return_value = "Dependencies::abcdefg"
        result = cdlib.get_mr_pathlist(mock_inst, mock_proj, mock_mr)
        self.assertEqual(result, ['include/linux/netdevice.h'])
        dep_label.return_value = "Dependencies::deadbeef"
        result = cdlib.get_mr_pathlist(mock_inst, mock_proj, mock_mr)
        self.assertEqual(result, ['include/linux/netdevice.h'])

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.cdlib.get_mr_pathlist')
    def test_reset_blocking_test_labels(self, get_paths, add_label):
        mock_inst = mock.Mock()
        mock_proj = mock.Mock()
        mock_mr = mock.Mock()
        mr_id = 2468
        mock_mr.iid = mr_id
        label_prefix = 'lnst'
        name = f'{label_prefix}::{defs.NEEDS_TESTING_SUFFIX}'
        get_paths.return_value = ['net/core/dev.c']
        owners_yaml = ("subsystems:\n"
                       " - subsystem: NETWORKING [GENERAL]\n"
                       "   labels:\n"
                       "     name: net\n"
                       "     readyForMergeDeps:\n"
                       "       - lnst\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - net/\n")
        owners_parser = owners.Parser(owners_yaml)
        cdlib.reset_blocking_test_labels(mock_inst, mock_proj, mock_mr, owners_parser)
        add_label.assert_called_with(mock_inst, mock_proj, mr_id, [name])

    @mock.patch('webhook.cdlib.extract_files')
    def test_is_rhdocs_commit(self, filelist):
        filelist.return_value = ['redhat/rhdocs/hugo_config.yaml']
        commit = mock.Mock()
        status = cdlib.is_rhdocs_commit(commit)
        self.assertTrue(status)
        filelist.return_value = ['redhat/rhdocs/info/owners.yaml']
        status = cdlib.is_rhdocs_commit(commit)
        self.assertTrue(status)
        filelist.return_value = ['include/linux/net_device.h']
        status = cdlib.is_rhdocs_commit(commit)
        self.assertFalse(status)
        filelist.return_value = ['scripts/do_something.bat']
        self.assertFalse(cdlib.is_rhdocs_commit(commit))
        filelist.return_value = ['redhat/rhdocs/scripts/do_something.bat']
        self.assertTrue(cdlib.is_rhdocs_commit(commit))
