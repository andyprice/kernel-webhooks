"""Webhook interaction tests."""
from unittest import TestCase

from webhook import pipelines


class TestPipelines(TestCase):
    """Tests for the pipelines library."""

    PIPE_MAP = {'trigger_pipeline': pipelines.PipelineType.KERNEL_LEGACY,
                'merge_request_regular': pipelines.PipelineType.KERNEL_LEGACY,
                'merge_request_realtime': pipelines.PipelineType.KERNEL_LEGACY,
                'merge_request_private':  pipelines.PipelineType.KERNEL_LEGACY,
                'merge_request_realtime_private': pipelines.PipelineType.KERNEL_LEGACY,
                'rhel9_private_merge_request': pipelines.PipelineType.KERNEL,
                'realtime_check': pipelines.PipelineType.KERNEL_RT_LEGACY,
                'realtime_check_regular': pipelines.PipelineType.KERNEL_RT_LEGACY,
                'realtime_check_private': pipelines.PipelineType.KERNEL_RT_LEGACY,
                'c9s_merge_request': pipelines.PipelineType.KERNEL,
                'c9s_realtime_check_merge_request': pipelines.PipelineType.KERNEL_RT,
                'c9s_rhel9_compat_merge_request': pipelines.PipelineType.KERNEL_SHADOW
                }

    def _test_pipeline(self, func, pipe_name, result):
        result_string = result.name if isinstance(result, pipelines.PipelineType) else result
        print(f"Testing {func.__name__}() with '{pipe_name}', expecting '{result_string}'...")
        self.assertIs(func(pipe_name), result)

    def test_get_pipeline_type(self):
        """Returns the expected PipelineType value for the given input."""
        func = pipelines.get_pipeline_type
        self._test_pipeline(func, '', pipelines.PipelineType.INVALID)
        self._test_pipeline(func, 'what', pipelines.PipelineType.INVALID)
        for pipe_name, pipe_type in self.PIPE_MAP.items():
            self._test_pipeline(func, pipe_name, pipe_type)

    def test_is_valid_bridge_job(self):
        """Returns True if the name matches a PipelineType other than INVALID."""
        func = pipelines.is_valid_bridge_job
        self._test_pipeline(func, '', False)
        self._test_pipeline(func, 'what', False)
        for pipe_name in self.PIPE_MAP:
            self._test_pipeline(func, pipe_name, True)

    def test_is_kernel_pipeline(self):
        """Returns True if the name matches a PipelineType of KERNEL or KERNEL_LEGACY."""
        func = pipelines.is_kernel_pipeline
        self._test_pipeline(func, '', False)
        self._test_pipeline(func, 'what', False)
        for pipe_name, pipe_type in self.PIPE_MAP.items():
            result = bool(pipe_type in (pipelines.PipelineType.KERNEL,
                                        pipelines.PipelineType.KERNEL_LEGACY))
            self._test_pipeline(func, pipe_name, result)

    def test_is_kernel_rt_pipeline(self):
        """Returns True if the name matches a PipelineType of KERNEL_RT or KERNEL_RT_LEGACY."""
        func = pipelines.is_kernel_rt_pipeline
        self._test_pipeline(func, '', False)
        self._test_pipeline(func, 'what', False)
        for pipe_name, pipe_type in self.PIPE_MAP.items():
            result = bool(pipe_type in (pipelines.PipelineType.KERNEL_RT,
                                        pipelines.PipelineType.KERNEL_RT_LEGACY))
            self._test_pipeline(func, pipe_name, result)

    def test_is_kernel_shadow_pipeline(self):
        """Returns True if the name matches a PipelineType of KERNEL_SHADOW."""
        func = pipelines.is_kernel_shadow_pipeline
        self._test_pipeline(func, '', False)
        self._test_pipeline(func, 'what', False)
        for pipe_name, pipe_type in self.PIPE_MAP.items():
            result = bool(pipe_type is pipelines.PipelineType.KERNEL_SHADOW)
            self._test_pipeline(func, pipe_name, result)
