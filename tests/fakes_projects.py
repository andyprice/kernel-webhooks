"""Fake Projects objects."""
from webhook import rh_metadata

BRANCH1 = rh_metadata.Branch(name='main',
                             component='kernel',
                             distgit_ref='c9s',
                             internal_target_release='9.1.0')

PROJECT1 = rh_metadata.Project(ids=[11223344],
                               name='centos-stream-9',
                               product='Red Hat Enterprise Linux 9',
                               branches=[BRANCH1.__dict__])

BRANCH2 = rh_metadata.Branch(name='main',
                             component='kernel',
                             distgit_ref='rhel-8.7.0',
                             internal_target_release='8.7.0')

BRANCH3 = rh_metadata.Branch(name='main-rt',
                             component='kernel-rt',
                             distgit_ref='rhel-8.7.0',
                             internal_target_release='8.7.0')

BRANCH4 = rh_metadata.Branch(name='8.6',
                             component='kernel',
                             distgit_ref='rhel-8.6.0',
                             zstream_target_release='8.6.0')

BRANCH5 = rh_metadata.Branch(name='8.6-rt',
                             component='kernel-rt',
                             distgit_ref='rhel-8.6.0',
                             zstream_target_release='8.6.0')

BRANCH6 = rh_metadata.Branch(name='8.5',
                             component='kernel',
                             distgit_ref='rhel-8.5.0',
                             zstream_target_release='8.5.0')

BRANCH7 = rh_metadata.Branch(name='8.5-rt',
                             component='kernel-rt',
                             distgit_ref='rhel-8.5.0',
                             zstream_target_release='8.5.0')

BRANCH8 = rh_metadata.Branch(name='8.8',
                             component='kernel',
                             distgit_ref='rhel-8.8.0',
                             internal_target_release='8.8-Beta')

BRANCH9 = rh_metadata.Branch(name='8.9',
                             component='kernel',
                             distgit_ref='rhel-8.9.0',
                             internal_target_release='8.8-Alpha')

PROJECT2 = rh_metadata.Project(ids=[12345, 56789],
                               name='rhel-8',
                               product='Red Hat Enterprise Linux 8',
                               branches=[BRANCH2.__dict__, BRANCH3.__dict__, BRANCH4.__dict__,
                                         BRANCH5.__dict__, BRANCH6.__dict__, BRANCH7.__dict__,
                                         BRANCH8.__dict__, BRANCH9.__dict__])


class FakeProjects(rh_metadata.Projects):
    """Provide a fake Projects object."""

    def __init__(self, projects=None):
        """Set up the projects."""
        self.__dict__['projects'] = projects if projects else [PROJECT1]
        # Set the expected Branch.project attribute hack.
        for project in self.projects:
            for branch in project.branches:
                object.__setattr__(branch, 'project', project)
