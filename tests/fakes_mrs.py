"""Fake MR objects."""
from unittest import mock

from tests import fakes_projects
from webhook.bughook import MR


def make_mr(namespace, mr_id, query_results_list=None, projects=None,
            is_dependency=False):
    """Help make an MR."""
    graphql = mock.Mock()
    results = [{'project': result} for result in query_results_list] if query_results_list else []
    graphql.execute_paged_query.side_effect = results if results else [None]
    if not projects:
        projects = fakes_projects.FakeProjects()
    return MR(graphql=graphql,
              projects=projects,
              namespace=namespace,
              mr_id=mr_id,
              is_dependency=is_dependency)


MR309_DICT = {'id': 'gid://gitlab/Project/11223344',
              'mr': {'description': 'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                                    'Depends: !303\n'
                                    'CVE: CVE-1235-13516\n',
                     'id': 'gid://gitlab/MergeRequest/21122221',
                     'state': 'opened',
                     'draft': False,
                     'targetBranch': 'main',
                     'headPipeline': {'finishedAt': None},
                     'files': [{'path': 'fs/ext/fs.c'}, {'path': 'lib/include/what.h'}],
                     'commits': {'pageInfo': {'hasNextPage': False, 'endCursor': 'Mw'},
                                 'nodes': [{'sha': '0aa467549b4e997d023c29f4d481aee01b9e9471',
                                            'description': 'Bugzilla: '
                                                           'https://bugzilla.redhat.com/1234567\n'
                                                           'CVE: CVE-1235-13516\n'},
                                           {'sha': 'e53eab9f887f784044ad32ef5c082695831d90d9',
                                            'description': 'Bugzilla: '
                                                           'https://bugzilla.redhat.com/1234567\n'
                                                           'CVE: CVE-1235-13516\n'},
                                           {'sha': '88cdd4035228dac16878eb907381afea6ceffeaa',
                                            'description': 'Bugzilla: '
                                                           'https://bugzilla.redhat.com/1234567\n'
                                                           'CVE: CVE-1235-13516\n'},
                                           {'sha': 'ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3',
                                            'description': 'Bugzilla: '
                                                           'https://bugzilla.redhat.com/2323232\n'},
                                           {'sha': 'f77278fcd9cef99358adc7f5e077be795a54ffca',
                                            'description': 'Bugzilla: '
                                                           'https://bugzilla.redhat.com/2323232\n'}]
                                 }}}

MR303_DICT = {'id': 'gid://gitlab/Project/11223344',
              'mr': {'description': 'Bugzilla: https://bugzilla.redhat.com/2323232\n',
                     'id': 'gid://gitlab/MergeRequest/262327',
                     'state': 'opened',
                     'draft': False,
                     'targetBranch': 'main',
                     'headPipeline': {'finishedAt': '2022-06-14T15:09:22Z'},
                     'files': [{'path': 'lib/include/what.h'}],
                     'commits': {'pageInfo': {'hasNextPage': False, 'endCursor': 'Mw'},
                                 'nodes': [{'sha': 'ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3',
                                            'description': 'Bugzilla: '
                                                           'https://bugzilla.redhat.com/2323232\n'},
                                           {'sha': 'f77278fcd9cef99358adc7f5e077be795a54ffca',
                                            'description': 'Bugzilla: '
                                                           'https://bugzilla.redhat.com/2323232\n'}]
                                 }}}

MR309 = make_mr('group/centos-stream-9', 309, query_results_list=[MR309_DICT, MR303_DICT])
MR303 = make_mr('group/centos-stream-9', 303, query_results_list=[MR303_DICT], is_dependency=True)
