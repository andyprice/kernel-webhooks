"""Tests for the defs."""
from unittest import TestCase

from webhook import defs


class TestBZStatus(TestCase):
    """Tests for the BZStatus enum."""

    def test_from_str(self):
        """Returns the BZStatus member whose name matches the input string, or UNKNOWN."""
        self.assertEqual(len(defs.BZStatus), 10)
        self.assertIs(defs.BZStatus.from_str('New'), defs.BZStatus.NEW)
        self.assertIs(defs.BZStatus.from_str('POST'), defs.BZStatus.POST)
        self.assertIs(defs.BZStatus.from_str('modified'), defs.BZStatus.MODIFIED)
        self.assertIs(defs.BZStatus.from_str('crazy'), defs.BZStatus.UNKNOWN)


class TestMrScope(TestCase):
    """Tests for the MrScope enum."""

    def test_mrscope_label(self):
        """Returns a label string."""
        prefix = 'Bugzilla'
        self.assertEqual(defs.MrScope.INVALID.label(prefix), f'{prefix}::Invalid')
        self.assertEqual(defs.MrScope.NEEDS_REVIEW.label(prefix),
                         f'{prefix}::{defs.NEEDS_REVIEW_SUFFIX}')
        self.assertEqual(defs.MrScope.READY_FOR_QA.label(prefix),
                         f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}')
        self.assertEqual(defs.MrScope.READY_FOR_MERGE.label(prefix),
                         f'{prefix}::{defs.READY_SUFFIX}')
        self.assertEqual(defs.MrScope.CLOSED.label(prefix), f'{prefix}::Closed')


class TestMrState(TestCase):
    """Tests for the MrState enum."""

    def test_mrstate_from_str(self):
        """Returns the MrState member whose name matches the given string."""
        self.assertIs(defs.MrState.from_str('closed'), defs.MrState.CLOSED)
        self.assertIs(defs.MrState.from_str('Locked'), defs.MrState.LOCKED)
        self.assertIs(defs.MrState.from_str('MERGED'), defs.MrState.MERGED)
        self.assertIs(defs.MrState.from_str('opened'), defs.MrState.OPENED)
        self.assertIs(defs.MrState.from_str('chicken'), defs.MrState.UNKNOWN)
