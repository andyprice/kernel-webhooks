"""Webhook interaction tests."""
from unittest import TestCase
from unittest import mock

from webhook import defs
from webhook import owners
from webhook import subsystems


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestSubsystems(TestCase):

    OWNERS_YAML = ("subsystems:\n"
                   " - subsystem: MEMORY MANAGEMENT\n"
                   "   labels:\n"
                   "     name: mm\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - include/linux/mm.h\n"
                   "          - include/linux/vmalloc.h\n"
                   "          - mm/\n"
                   " - subsystem: NETWORKING\n"
                   "   labels:\n"
                   "     name: net\n"
                   "     readyForMergeDeps:\n"
                   "       - lnst\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - include/linux/net.h\n"
                   "          - include/net/\n"
                   "          - net/\n"
                   " - subsystem: XDP\n"
                   "   labels:\n"
                   "     name: xdp\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - kernel/bpf/devmap.c\n"
                   "          - include/net/page_pool.h\n"
                   "       includeRegexes:\n"
                   "          - xdp\n")

    def test_user_wants_notification(self):
        user_data = {'all': ['net/'],
                     '8.y': ['include/net/bonding.h'],
                     '8.2': ['include/net/*'],
                     '8.1': ['*/net/*']}

        path_list = ['net/core/dev.c']
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['networking/ipv8.c']
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bond_3ad.h']
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding.h']
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding/bonding.h']
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding/bonding.h']
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.1'))

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_set_blocking_test_labels(self, is_prod):
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.labels = []
        blabels = []
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            # MR is in draft state, don't add label
            mock_mr.draft = True
            results = subsystems.set_blocking_test_labels(mock_mr, blabels)
            self.assertIn('Not adding blocking test label to MR 2 in draft state', logs.output[-1])
            self.assertEqual(results, [])
            # No label to add
            mock_mr.draft = False
            results = subsystems.set_blocking_test_labels(mock_mr, blabels)
            self.assertIn('No blocking test labels to add.', logs.output[-1])
            self.assertEqual(results, [])
            # Add blocking label
            blabels = [['lnst']]
            results = subsystems.set_blocking_test_labels(mock_mr, blabels)
            self.assertEqual(len(results), 1)
            self.assertEqual(results[0], 'lnst::NeedsTesting')
            # Blocking label already exists, don't overwrite it
            mock_mr.labels = ['lnst::OK']
            results = subsystems.set_blocking_test_labels(mock_mr, blabels)
            self.assertIn('A blocking test label already exists (lnst::OK).', logs.output[-1])
            self.assertEqual(results, [])

    @mock.patch('os.listdir')
    @mock.patch('webhook.common.load_yaml_data')
    @mock.patch('webhook.subsystems.user_wants_notification')
    def test_do_usermapping(self, mock_user_wants, mock_loader, mock_listdir):
        # Cannot get file listing.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_listdir.side_effect = PermissionError
            result = subsystems.do_usermapping('8.y', [], '/repo')
            self.assertEqual(result, [])
            self.assertIn('Problem listing path', logs.output[-1])
            mock_user_wants.assert_not_called()

        # Error loading user data.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_listdir.side_effect = None
            mock_listdir.return_value = ['user1']
            mock_loader.side_effect = [None]
            result = subsystems.do_usermapping('8.y', [], '/repo')
            self.assertEqual(result, [])
            self.assertIn("Error loading user data from path '/repo/users/user1'.",
                          logs.output[-1])
            mock_user_wants.assert_not_called()

        mock_listdir.return_value = ['user1', 'user2', 'user3']
        mock_loader.side_effect = None
        path_list = ['include/net/bonding.h', 'net/core/dev.c']

        user1_data = {'all': ['include/net/bonding.h']}
        user2_data = {'all': ['drivers/net/bonding/']}
        user3_data = {'all': ['include/net/bonding.h']}
        mock_loader.side_effect = [user1_data, user2_data, user3_data]
        mock_user_wants.side_effect = [True, False, True]
        result = subsystems.do_usermapping('8.y', path_list, '/repo')
        call_list = [mock.call(user1_data, path_list, '8.y'),
                     mock.call(user2_data, path_list, '8.y'),
                     mock.call(user3_data, path_list, '8.y')]

        self.assertEqual(mock_user_wants.call_count, 3)
        mock_user_wants.assert_has_calls(call_list, any_order=True)
        self.assertEqual(sorted(result), ['user1', 'user3'])

    @mock.patch('webhook.common.update_webhook_comment')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_post_notifications(self, add_comment):
        mock_inst = mock.Mock()
        mock_inst.user.username = "shadowman"
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.participants.return_value = [{'username': 'user1'},
                                             {'username': 'user2'},
                                             {'username': 'user3'}]

        # No one new to notify.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            user_list = ['user1', 'user3']
            subsystems.post_notifications(mock_inst, mock_mr, user_list,
                                          'https://gitlab.com/project1')
            self.assertIn('No one new to notify.', logs.output[-1])
            mock_mr.participants.assert_called()
            add_comment.assert_not_called()

        # Create a note.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            user_list = ['user1', 'user2', 'user4', 'user5']
            template = defs.NOTIFICATION_TEMPLATE
            note_text = template.format(header=defs.NOTIFICATION_HEADER,
                                        users='@user4 @user5',
                                        project='https://gitlab.com/project1')
            subsystems.post_notifications(mock_inst, mock_mr, user_list,
                                          'https://gitlab.com/project1')
            self.assertIn('Posting notification on MR 2:', logs.output[-1])
            mock_mr.participants.assert_called()
            add_comment.assert_called_once()
            add_comment.assert_called_with(mock_mr, "shadowman",
                                           defs.NOTIFICATION_HEADER, note_text)

    def test_make_labels(self):
        topics = ['include', 'net', 'lnst::NeedsTesting']
        expected_list = [f'{defs.SUBSYS_LABEL_PREFIX}:include',
                         f'{defs.SUBSYS_LABEL_PREFIX}:net',
                         'lnst::NeedsTesting']

        result = subsystems.make_labels(topics)
        self.assertEqual(len(result), 3)
        self.assertEqual(result, expected_list)

    def test_get_current_subsystems_from_labels(self):
        mock_mr = mock.Mock()
        mock_mr.labels = ['Subsystem:net', 'Subsystem:ethernet', 'Subsystem:wireless']
        output = subsystems.get_current_subsystems_from_labels(mock_mr)
        self.assertEqual(output, ['net', 'ethernet', 'wireless'])

    def test_get_stale_subsystems(self):
        old = ['net', 'ethernet', 'wireless']
        new = ['net', 'ethernet']
        output = subsystems.get_stale_subsystems(old, new)
        self.assertEqual(output, ['wireless'])

    @mock.patch('webhook.common.remove_labels_from_merge_request')
    def test_remove_stale_subsystem_labels(self, mock_remove):
        mock_proj = mock.Mock()
        mr_id = '100'
        stale = ['wireless']
        subsystems.remove_stale_subsystem_labels(mock_proj, mr_id, stale)
        mock_remove.assert_called_with(mock_proj, mr_id, ['Subsystem:wireless'])

    @mock.patch('webhook.common.update_webhook_comment', mock.Mock())
    @mock.patch('webhook.common.remove_labels_from_merge_request')
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.subsystems.post_notifications')
    @mock.patch('webhook.subsystems.do_usermapping')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.cdlib.get_mr_pathlist')
    @mock.patch('webhook.common.process_config_items')
    def test_process_mr(self, mock_process_config, mock_pathlist, mock_add_label, mock_usermapping,
                        mock_poster, mock_gl, mock_remove):
        project_url = 'https://gitlab.com/project123'
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.commits.return_value = [1]
        mock_mr.target_branch = 'main'
        mock_project = mock.Mock()
        mock_project.namespace = {'name': '8.y'}
        mock_gl.projects.get.return_value = mock_project
        mock_project.mergerequests.get.return_value = mock_mr
        mock_mr.labels = []
        mock_pathlist.return_value = []
        mock_process_config.return_value = ([], [])
        mock_usermapping.return_value = []

        msg = mock.Mock()
        msg.payload = {}
        msg.payload['project'] = {'id': 1, 'path_with_namespace': 'g/p'}
        msg.payload['changes'] = {'labels': {'previous': [],
                                             'current': [{'title': 'Subsystem:'}]
                                             }
                                  }
        msg.payload['object_attributes'] = {'iid': 2, 'action': 'open'}

        # No path changes.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            owners_parser = owners.Parser(None)
            subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url, '/linux')
            self.assertIn('MR 2 does not report any changed files, exiting.', logs.output[-1])
            mock_usermapping.assert_not_called()
            mock_poster.assert_not_called()
            mock_add_label.assert_not_called()

        owners_parser = owners.Parser(self.OWNERS_YAML)

        # No label to add.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            mock_pathlist.return_value = ['weirdfile.bat']
            subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url, '/linux')
            self.assertIn('No labels to add.', ' '.join(logs.output))
            mock_add_label.assert_not_called()
            mock_usermapping.assert_called()

        # Label, no users to map.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            mock_pathlist.return_value = ['net/core/dev.c']
            name = f'{defs.SUBSYS_LABEL_PREFIX}:net'
            tname = f'lnst::{defs.NEEDS_TESTING_SUFFIX}'
            mock_usermapping.return_value = []
            subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url, '/linux')
            mock_add_label.assert_called_with(mock_gl, mock_project, 2, [name, tname])
            mock_usermapping.assert_called_with('8.y', mock_pathlist.return_value, '/data/repo')
            mock_poster.assert_not_called()
            self.assertIn('No new users to notify @ for MR 2.', logs.output[-1])

        # Labels & user mapped.
        mock_usermapping.return_value = ['user1']
        subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url, '/linux')
        mock_add_label.assert_called_with(mock_gl, mock_project, 2, [name, tname])
        mock_usermapping.assert_called_with('8.y', mock_pathlist.return_value, '/data/repo')
        mock_poster.assert_called_with(mock_gl, mock_mr, ['user1'], project_url)

        # Stale labels to remove.
        mock_mr.labels = ['Subsystem:net', 'Subsystem:ethernet']
        mock_pathlist.return_value = ['net/core/dev.c']
        subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url, '/linux')
        mock_remove.assert_called_with(mock_project, mock_mr.iid, ['Subsystem:ethernet'])

        # Config files touched, add Configuration label
        mock_mr.labels = []
        mock_pathlist.return_value = ['redhat/configs/debug/CONFIG_FOO']
        mock_process_config.return_value = ([], [defs.CONFIG_LABEL])
        subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url, '/linux')
        mock_add_label.assert_called_with(mock_gl, mock_project, 2, [defs.CONFIG_LABEL])
