"""Webhook interaction tests."""
from copy import deepcopy
from unittest import TestCase
from unittest import mock

from tests import fakes
from webhook import ckihook
from webhook import defs

CHANGES = {'labels': {'previous': [{'title': f'{defs.CKI_KERNEL_RT_PREFIX}::Running'},
                                   {'title': f'{defs.CKI_KERNEL_PREFIX}::Running'}
                                   ],
                      'current': [{'title': f'{defs.CKI_KERNEL_RT_PREFIX}::Canceled'}]
                      }
           }

MR_PAYLOAD = {'user': {'username': 'test_user'},
              'project': {'path_with_namespace': 'group/project'},
              'object_attributes': {'iid': 123,
                                    'head_pipeline_id': None,
                                    'target_branch': '8.6'},
              'labels': [{'title': 'hey'}],
              'changes': {}
              }


class TestPipeStatus(TestCase):
    """Tests for the PipeStatus enum."""

    def test_title(self):
        """Title() should capitalize() all Statuses except OK."""
        self.assertEqual(ckihook.PipelineStatus.FAILED.title(), 'Failed')
        self.assertEqual(ckihook.PipelineStatus.CREATED.title(), 'Running')
        self.assertEqual(ckihook.PipelineStatus.OK.title(), 'OK')


class TestPipelineResult(TestCase):
    """Tests for the PipelineResult dataclass."""

    STAGES = [{'name': 'prepare', 'jobs': {'nodes': [{'status': 'SUCCESS'},
                                                     {'status': 'SUCCESS'}]}},
              {'name': 'build', 'jobs': {'nodes': [{'status': 'SUCCESS'},
                                                   {'status': 'SUCCESS'}]}},
              {'name': 'build-tools', 'jobs': {'nodes': [{'status': 'SUCCESS'},
                                                         {'status': 'RUNNING'}]}},
              {'name': 'test', 'jobs': {'nodes': [{'status': 'PENDING'},
                                                  {'status': 'PENDING'}]}},
              {'name': 'results', 'jobs': {'nodes': [{'status': 'PENDING'},
                                                     {'status': 'PENDING'}]}},
              ]

    def test_init_with_dict(self):
        """Object properties should be set."""
        stages = deepcopy(self.STAGES)
        # Sets label
        input_dict = {'status': 'pending',
                      'sourceJob': {'name': 'rhel8_realtime_check_merge_request'},
                      'stages': {'nodes': stages}
                      }
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.RUNNING)
        self.assertEqual(result.label, 'CKI_RT::Running')
        self.assertEqual(result.type, ckihook.PipelineType.KERNEL_RT)
        # Sets failed label
        input_dict['status'] = 'failed'
        stages[-2]['jobs']['nodes'].append({'status': 'FAILED'})
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.FAILED)
        self.assertEqual(result.label, 'CKI_RT::Failed::test')
        self.assertEqual(result.type, ckihook.PipelineType.KERNEL_RT)
        # Sets no label
        input_dict['sourceJob']['name'] = 'c9s_rhel9_compat_merge_request'
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.FAILED)
        self.assertEqual(result.label, '')
        self.assertEqual(result.type, ckihook.PipelineType.KERNEL_SHADOW)

    def test_get_failed_stage(self):
        """Returns a stage name if status is FAILED and a failed job is found, or None."""
        stages = deepcopy(self.STAGES)
        # No failed jobs
        self.assertEqual(ckihook.PipelineResult._get_failed_stage(stages), '')
        # Test job failed.
        stages[1]['jobs']['nodes'].append({'status': 'FAILED'})
        stages[-2]['jobs']['nodes'].append({'status': 'FAILED'})
        self.assertEqual(ckihook.PipelineResult._get_failed_stage(stages), 'test')


class TestFetchCKI(TestCase):
    """Validate FetchCKI object behaviour."""

    def test_FetchCKI_init(self):
        """Sets the expected properties."""
        user = 'test_user'
        namespace = 'group/project'
        mr_id = 123
        result = ckihook.FetchCKI(user, namespace, mr_id)
        self.assertEqual(result.event_user, user)
        self.assertEqual(result.namespace, namespace)
        self.assertEqual(result.mr_id, mr_id)

    @mock.patch('webhook.ckihook.FetchCKI._validate_query_results')
    def test_FetchCKI_query(self, mock_validate_query):
        """Runs a query and returns the output of validate_pipeline_query_results()."""
        user = 'test_user'
        namespace = 'group/project'
        mr_id = 123
        mycki = ckihook.FetchCKI(user, namespace, mr_id)
        mycki.execute_query = mock.Mock()
        query_params = {'namespace': namespace, 'mr_id': str(mr_id)}
        result = mycki.run_query()
        mycki.execute_query.assert_called_once_with(ckihook.PIPELINE_QUERY, query_params,
                                                    check_keys=ckihook.PIPELINE_QUERY_CHECK_KEYS,
                                                    check_user=user)
        mock_validate_query.assert_called_once_with(mycki.execute_query.return_value)
        self.assertEqual(result, mock_validate_query.return_value)

    def test_FetchCKI_validate_query_results(self):
        """Returns an empty list when query results do not have the necessary details."""
        # Nada.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            self.assertEqual(ckihook.FetchCKI._validate_query_results([]), [])
            self.assertIn('Nothing to parse.', logs.output[-1])

        # No MR.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            self.assertEqual(ckihook.FetchCKI._validate_query_results({'project': {}}), [])
            self.assertIn('Merge request does not exist', logs.output[-1])

        # No head pipeline.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            graphql = {'project': {'mr': {'headPipeline': {}}}}
            self.assertEqual(ckihook.FetchCKI._validate_query_results(graphql), [])
            self.assertIn('MR does not have headPipeline set.', logs.output[-1])

        # No downstream notes.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            graphql = {'project': {'mr': {'headPipeline': {'downstream': {'nodes': []}}}}}
            self.assertEqual(ckihook.FetchCKI._validate_query_results(graphql), [])
            self.assertIn('headPipeline does not have any downstream nodes set', logs.output[-1])


class TestHelpers(TestCase):
    """Test helper functions."""

    @mock.patch('webhook.ckihook.PipelineResult')
    def test_map_pipeline_query_results_good(self, mock_PipelineResult):
        """Passes downstream nodes to generate PipelineResult objects and returns them as a list."""
        nodes = [1, 2]
        graphql = {'project': {'mr': {'headPipeline': {'downstream': {'nodes': nodes}}}}}
        results = ckihook.map_pipeline_query_results(graphql)
        self.assertEqual(mock_PipelineResult.call_count, 2)
        self.assertEqual(mock_PipelineResult.call_args_list[0].kwargs['api_dict'], 1)
        self.assertEqual(mock_PipelineResult.call_args_list[1].kwargs['api_dict'], 2)
        self.assertEqual(len(results), 2)

    @mock.patch('webhook.ckihook.get_instance')
    @mock.patch('webhook.common.add_label_to_merge_request')
    def test_add_labels(self, mock_add_label, mock_get_instance):
        """Gets a gl_instance/project and calls add_label_to_merge_request."""
        namespace = 'group/project'
        mr_id = 123
        labels = ['CKI::Running', 'CKI_RT::OK']
        ckihook.add_labels(namespace, mr_id, labels)
        mock_get_instance.projects.get.called_once_with(namespace)
        mock_add_label.called_once_with(mock_get_instance,
                                        mock_get_instance.projects.get.return_value, mr_id, labels,
                                        remove_scoped=True)

    def test_cki_label_changed(self):
        """Returns True if any CKI labels are in the changes list."""
        changes = deepcopy(CHANGES)
        self.assertTrue(ckihook.cki_label_changed(changes))
        self.assertFalse(ckihook.cki_label_changed({}))

    @mock.patch('webhook.ckihook.FetchCKI')
    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_compute_new_labels(self, mock_missing_check, mock_map_results, mock_fetch):
        """Returns a list of labels if any changed."""
        user = 'test_user'
        namespace = 'group/project'
        mr_id = 123

        # CKI changed so we compute new labels.
        mock_map_results.return_value = [mock.Mock(label='CKI::OK'),
                                         mock.Mock(label='CKI_RT::Failed::merge')]
        mock_missing_check.return_value = []
        results = ckihook.compute_new_labels(user, namespace, mr_id)
        mock_fetch.assert_called_with(user, namespace, mr_id)
        mock_missing_check.assert_called_once_with(['CKI::OK', 'CKI_RT::Failed::merge'])
        self.assertEqual(results, ['CKI::OK', 'CKI_RT::Failed::merge'])

    def test_check_for_missing_labels(self):
        """Returns a list with the ::Missing CKI labels."""
        # Input list has neither, return both.
        missing_labels = [f'{defs.CKI_KERNEL_PREFIX}::Missing',
                          f'{defs.CKI_KERNEL_RT_PREFIX}::Missing']
        self.assertEqual(ckihook.check_for_missing_labels([]), missing_labels)

        # Input list has one, return the other.
        missing_labels = [f'{defs.CKI_KERNEL_RT_PREFIX}::Missing']
        self.assertEqual(ckihook.check_for_missing_labels([f'{defs.CKI_KERNEL_PREFIX}::Running']),
                         missing_labels)

        # Has both, return an empty list.
        self.assertEqual(ckihook.check_for_missing_labels([f'{defs.CKI_KERNEL_PREFIX}::Running',
                                                           f'{defs.CKI_KERNEL_RT_PREFIX}::OK']),
                         [])

    @mock.patch('webhook.ckihook.FetchCKI')
    @mock.patch('webhook.ckihook.get_downstream_pipeline_branch')
    @mock.patch('webhook.common.cancel_pipeline')
    @mock.patch('webhook.common.create_mr_pipeline')
    def test_process_possible_branch_change_false(self, mock_create, mock_cancel,
                                                  mock_get_ds_branch, mock_fetch):
        """Returns False because the payload does not indicate a branch change."""
        user = 'user'
        namespace = 'group/project'
        mr_id = 123
        payload = deepcopy(MR_PAYLOAD)
        msg = mock.Mock(payload=payload)

        # No head pipeline ID
        self.assertIs(payload['object_attributes']['head_pipeline_id'], None)
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), False)

        # 'merge_status' not in 'changes'.
        payload['object_attributes']['head_pipeline_id'] = 56789
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), False)

        mock_fetch.assert_not_called()
        mock_get_ds_branch.assert_not_called()
        mock_cancel.assert_not_called()
        mock_create.assert_not_called()

        # downstream pipeline 'branch' matches mr target branch
        payload['changes']['merge_status'] = {'current': 'unchecked'}
        mock_get_ds_branch.return_value = payload['object_attributes']['target_branch']
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), False)
        mock_fetch.assert_called_once_with(user, namespace, mr_id)
        mock_get_ds_branch.assert_called_once_with(msg.gl_instance.return_value,
                                                   mock_fetch.return_value.run_query.return_value)
        mock_cancel.assert_not_called()
        mock_create.assert_not_called()

        # downstream pipeline 'branch' not found.
        mock_fetch.reset_mock()
        mock_get_ds_branch.reset_mock()
        payload['changes']['merge_status'] = {'current': 'unchecked'}
        mock_get_ds_branch.return_value = None
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), False)
        mock_fetch.assert_called_once_with(user, namespace, mr_id)
        mock_get_ds_branch.assert_called_once_with(msg.gl_instance.return_value,
                                                   mock_fetch.return_value.run_query.return_value)
        mock_cancel.assert_not_called()
        mock_create.assert_not_called()

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.ckihook.FetchCKI')
    @mock.patch('webhook.ckihook.get_downstream_pipeline_branch')
    @mock.patch('webhook.common.cancel_pipeline')
    @mock.patch('webhook.common.create_mr_pipeline')
    def test_process_possible_branch_change_true(self, mock_create, mock_cancel,
                                                 mock_get_ds_branch, mock_fetch):
        """Returns True when a change is detected and pipeline is canceled/triggered."""
        user = 'user'
        namespace = 'group/project'
        mr_id = 123
        payload = deepcopy(MR_PAYLOAD)
        msg = mock.Mock(payload=payload)
        payload['object_attributes']['head_pipeline_id'] = 56789
        payload['changes']['merge_status'] = {'current': 'unchecked'}

        # pipeline_branch_matches() says the branches differ so cancel/retrigger and return True
        mock_get_ds_branch.return_value = 'main'
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), True)
        mock_get_ds_branch.assert_called_once_with(msg.gl_instance.return_value,
                                                   mock_fetch.return_value.run_query.return_value)
        mock_cancel.assert_called_once_with(msg.gl_instance.return_value.projects.get.return_value,
                                            payload['object_attributes']['head_pipeline_id'])
        wow = msg.gl_instance.return_value.projects.get.return_value.mergerequests.get.return_value
        mock_create.assert_called_once_with(wow)

    def test_get_downstream_pipeline_branch(self):
        """Returns the 'branch' pipeline var value, or None."""
        ds_project_id = 23456
        ds_pipeline_id = 87654
        nodes = [{'id': f'gid://gitlab/Ci::Pipeline/{ds_pipeline_id}',
                  'project': {'id': f'gid://gitlab/Project/{ds_project_id}'}
                  }]
        p_data = {'project': {'mr': {'headPipeline': {'downstream': {'nodes': nodes}}}}}
        mock_gl_instance = fakes.FakeGitLab()
        mock_gl_instance.add_project(ds_project_id)
        ds_project = mock_gl_instance.projects.get(ds_project_id)

        # No 'branch' in pipeline vars.
        ds_pipeline_vars = [{'key': 'what', 'value': 'huh'}]
        ds_project.add_pipeline(ds_pipeline_id, variables=ds_pipeline_vars)
        self.assertIs(ckihook.get_downstream_pipeline_branch(mock_gl_instance, p_data), None)

        # A 'branch' in pipeline vars.
        ds_pipeline_vars = [{'key': 'branch', 'value': '8.4'}]
        ds_project.add_pipeline(ds_pipeline_id, variables=ds_pipeline_vars)
        self.assertEqual(ckihook.get_downstream_pipeline_branch(mock_gl_instance, p_data), '8.4')


class TestMRHandler(TestCase):
    """Tests for the MR event handler."""

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.process_possible_branch_change')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_label_changed(self, mock_check_for_missing, mock_branch_change,
                                            mock_fetch_new_labels, mock_cki_label_changed,
                                            mock_add_labels):
        """Calls add_labels with the results of cki_label_changed."""
        payload = deepcopy(MR_PAYLOAD)
        payload['changes'] = CHANGES
        msg = mock.Mock(payload=payload)
        mock_branch_change.return_value = False
        ckihook.process_mr_event(msg)
        mock_cki_label_changed.assert_called_once_with(payload['changes'])
        mock_branch_change.assert_called_once_with('test_user', 'group/project', 123, msg)
        mock_fetch_new_labels.assert_called_once_with('test_user', 'group/project', 123)
        mock_add_labels.assert_called_once_with('group/project', 123,
                                                mock_fetch_new_labels.return_value)
        mock_check_for_missing.assert_not_called()

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_rt_waived(self, mock_check_for_missing, mock_compute_new_labels,
                                        mock_cki_label_changed, mock_add_labels):
        """Calls add_labels with the results of cki_label_changed minus CKI_RT."""
        payload = deepcopy(MR_PAYLOAD)
        payload['labels'] = [{'title': 'CKI_RT::Waived'}]
        payload['changes'] = CHANGES
        msg = mock.Mock(payload=payload)
        mock_compute_new_labels.return_value = ['CKI::OK', 'CKI_RT::Failed::merge']
        ckihook.process_mr_event(msg)
        mock_cki_label_changed.assert_called_once_with(payload['changes'])
        mock_compute_new_labels.assert_called_once_with('test_user', 'group/project', 123)
        mock_add_labels.assert_called_once_with('group/project', 123, ['CKI::OK'])
        mock_check_for_missing.assert_not_called()

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_check_for_missing(self, mock_check_for_missing,
                                                mock_fetch_new_labels, mock_cki_label_changed,
                                                mock_add_labels):
        """No label changes, calls check_for_missing and add_labels."""
        payload = deepcopy(MR_PAYLOAD)
        msg = mock.Mock(payload=payload)
        mock_cki_label_changed.return_value = []
        ckihook.process_mr_event(msg)
        mock_cki_label_changed.assert_called_once_with({})
        mock_fetch_new_labels.assert_not_called()
        mock_check_for_missing.assert_called_once_with(['hey'])
        mock_add_labels.assert_called_once_with('group/project', 123,
                                                mock_check_for_missing.return_value)

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_no_action(self, mock_check_for_missing, mock_fetch_new_labels,
                                        mock_cki_label_changed, mock_add_labels):
        """No label changes and no missing CKI labels, nothing to do."""
        payload = deepcopy(MR_PAYLOAD)
        msg = mock.Mock(payload=payload)
        mock_cki_label_changed.return_value = []
        mock_check_for_missing.return_value = []
        ckihook.process_mr_event(msg)
        mock_cki_label_changed.assert_called_once_with({})
        mock_fetch_new_labels.assert_not_called()
        mock_check_for_missing.assert_called_once_with(['hey'])
        mock_add_labels.assert_not_called()

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.process_possible_branch_change')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_branch_changed(self, mock_check_for_missing, mock_branch_change,
                                             mock_fetch_new_labels, mock_cki_label_changed,
                                             mock_add_labels):
        """Stops when process_possible_branch_change() returns True."""
        payload = deepcopy(MR_PAYLOAD)
        payload['changes'] = CHANGES
        msg = mock.Mock(payload=payload)
        mock_branch_change.return_value = True
        ckihook.process_mr_event(msg)
        mock_branch_change.assert_called_once_with('test_user', 'group/project', 123, msg)
        mock_cki_label_changed.assert_not_called()
        mock_fetch_new_labels.assert_not_called()
        mock_add_labels.assert_not_called()
        mock_branch_change.assert_called_once_with('test_user', 'group/project', 123, msg)


class TestPipelineHandler(TestCase):
    """Tests for the Pipeline event handler."""

    PIPELINE_PAYLOAD = {'user': {'username': 'test_user'},
                        'project': {'path_with_namespace': 'group/project'},
                        'object_attributes': {'id': 12345,
                                              'status': 'running',
                                              'stages': ['prepare', 'merge', 'build', 'test'],
                                              'variables': []
                                              },
                        'builds': []
                        }

    VARIABLES = [{'key': 'mr_url', 'value': 'https://gitlab.com/group/project/-/merge_requests/10'},
                 {'key': 'trigger_job_name', 'value': 'merge_request_regular'}]

    @mock.patch('webhook.ckihook.PipelineResult', wraps=ckihook.PipelineResult)
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_no_vars(self, mock_add_labels, mock_PipelineResult):
        """Doesn't do anything."""
        # No mr_url, nothing to do.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            msg = mock.Mock(payload=payload)
            ckihook.process_pipeline_event(msg)
            self.assertIn('Event did not contain the expected variables', logs.output[-1])
            mock_add_labels.assert_not_called()
            mock_PipelineResult.assert_not_called()

        # Has mr_url, but retrigger is 'true'
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            payload['object_attributes']['variables'] = deepcopy(self.VARIABLES)
            payload['object_attributes']['variables'].append({'key': 'retrigger', 'value': 'true'})
            msg = mock.Mock(payload=payload)
            ckihook.process_pipeline_event(msg)
            self.assertIn('Event did not contain the expected variables', logs.output[-1])
            mock_add_labels.assert_not_called()
            mock_PipelineResult.assert_not_called()

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.FetchCKI')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_sets_label(self, mock_add_labels, mock_FetchCKI,
                                               mock_map_results):
        """Calls FetchCKI and sets a label."""
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            payload['object_attributes']['variables'] = deepcopy(self.VARIABLES)
            msg = mock.Mock(payload=payload)
            mock_PipelineResult = mock.Mock(label='CKI::Running')
            mock_PipelineResult.name = 'merge_request_regular'
            mock_map_results.return_value = [mock_PipelineResult]
            ckihook.process_pipeline_event(msg)
            self.assertIn('Setting label CKI::Running', logs.output[-1])
            mock_add_labels.assert_called_once_with('group/project', 10, ['CKI::Running'])

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.FetchCKI')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_failed(self, mock_add_labels, mock_FetchCKI, mock_map_results):
        """Calls PipelineResult and sets a label."""
        # If the event status is Failed get the failed stage and set a failed label.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            payload['object_attributes']['variables'] = deepcopy(self.VARIABLES)
            payload['object_attributes']['status'] = 'failed'
            payload['builds'] += [{'stage': 'build', 'status': 'failed'},
                                  {'stage': 'test', 'status': 'skipped'}]
            msg = mock.Mock(payload=payload)
            mock_PipelineResult = mock.Mock(label='CKI::Failed::build')
            mock_PipelineResult.name = 'merge_request_regular'
            mock_map_results.return_value = [mock_PipelineResult]
            ckihook.process_pipeline_event(msg)
            self.assertIn('Setting label CKI::Failed::build', logs.output[-1])
            mock_add_labels.assert_called_once_with('group/project', 10, ['CKI::Failed::build'])

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.FetchCKI')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_failed_is_running(self, mock_add_labels, mock_FetchCKI,
                                                      mock_map_results):
        """Calls PipelineResult and sets a label."""
        # If the event status is Failed but there are no failed builds then set status to RUNNING.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            payload['object_attributes']['variables'] = deepcopy(self.VARIABLES)
            payload['object_attributes']['status'] = 'failed'
            msg = mock.Mock(payload=payload)
            mock_PipelineResult = mock.Mock(label='CKI::Running')
            mock_PipelineResult.name = 'merge_request_regular'
            mock_map_results.return_value = [mock_PipelineResult]
            ckihook.process_pipeline_event(msg)
            self.assertIn('Setting label CKI::Running', logs.output[-1])
            mock_add_labels.assert_called_once_with('group/project', 10, ['CKI::Running'])

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.FetchCKI')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_no_label(self, mock_add_labels, mock_FetchCKI,
                                             mock_map_results):
        """Calls PipelineResult and does not set a label.."""
        # An event for a pipeline that we don't associate with either CKI or CKI_RT labels.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            variables = deepcopy(self.VARIABLES)
            variables[1]['value'] = 'c9s_rhel9_compat_merge_request'
            payload['object_attributes']['variables'] = variables
            msg = mock.Mock(payload=payload)
            mock_PipelineResult = mock.Mock(label=None)
            mock_PipelineResult.name = 'c9s_rhel9_compat_merge_request'
            mock_map_results.return_value = [mock_PipelineResult]
            ckihook.process_pipeline_event(msg)
            self.assertIn('No label to set for this pipeline', logs.output[-1])
            mock_add_labels.assert_not_called()


class TestNoteHandler(TestCase):
    """Tests for the Note event handler."""

    NOTE_PAYLOAD = {'user': {'username': 'test_user'},
                    'object_attributes': {'note': ''},
                    'project': {'path_with_namespace': 'group/project'},
                    'merge_request': {'iid': 123}
                    }

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.compute_new_labels')
    def test_process_note_event(self, mock_fetch_new_labels, mock_add_labels):
        """Runs compute_new_labels and optionally add_labels if there is a proper request."""
        payload = deepcopy(self.NOTE_PAYLOAD)
        payload['object_attributes']['note'] = 'request-cki-evaluation'
        mock_msg = mock.Mock(payload=payload)
        ckihook.process_note_event(mock_msg)
        mock_fetch_new_labels.assert_called_once_with(payload['user']['username'],
                                                      payload['project']['path_with_namespace'],
                                                      payload['merge_request']['iid'])
        mock_add_labels.assert_called_once_with(payload['project']['path_with_namespace'],
                                                payload['merge_request']['iid'],
                                                mock_fetch_new_labels.return_value)

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.compute_new_labels')
    def test_process_note_event_bad_note(self, mock_fetch_new_labels, mock_add_labels):
        """Does nothing when force_webhook_evaluation returns False."""
        payload = deepcopy(self.NOTE_PAYLOAD)
        payload['object_attributes']['note'] = 'a random comment'
        mock_msg = mock.Mock(payload=payload)
        ckihook.process_note_event(mock_msg)
        mock_fetch_new_labels.assert_not_called()
        mock_add_labels.assert_not_called()
