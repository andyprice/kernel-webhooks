"""Pipeline helper functions."""
from enum import IntEnum
from enum import auto

# Use of KERNEL_PIPELINES and KERNEL_RT_PIPELINES is deprecated,
# these are old pipeline names that are being phased out
KERNEL_PIPELINES = ['trigger_pipeline',
                    'merge_request_regular',
                    'merge_request_realtime',
                    'merge_request_private',
                    'merge_request_realtime_private',
                    'rhel9_private_merge_request']
KERNEL_RT_PIPELINES = ['realtime_check',
                       'realtime_check_regular',
                       'realtime_check_private']


class PipelineType(IntEnum):
    """Types of Pipelines we might see in our Merge Requests."""

    INVALID = 0
    KERNEL = auto()
    KERNEL_LEGACY = auto()
    KERNEL_RT = auto()
    KERNEL_RT_LEGACY = auto()
    KERNEL_SHADOW = auto()


def get_pipeline_type(name):
    """Return the PipelineType that corresponds to the given name."""
    ptype = PipelineType.INVALID
    if not name:
        return ptype
    if name.endswith("_compat_merge_request"):
        ptype = PipelineType.KERNEL_SHADOW
    elif name.endswith("_merge_request") or name.endswith("_merge_request_private"):
        ptype = PipelineType.KERNEL_RT if "realtime_check" in name else PipelineType.KERNEL
    if ptype:
        return ptype
    # Deprecated legacy pipelines
    if name in KERNEL_PIPELINES:
        ptype = PipelineType.KERNEL_LEGACY
    elif name in KERNEL_RT_PIPELINES:
        ptype = PipelineType.KERNEL_RT_LEGACY
    return ptype


def is_valid_bridge_job(name):
    """Return True if the name does not match PipelineType.INVALID, else False."""
    return bool(get_pipeline_type(name))


def is_kernel_pipeline(name):
    """Return True if the name matches PipelineType.KERNEL or KERNEL_LEGACY, else False."""
    return bool(get_pipeline_type(name) in (PipelineType.KERNEL, PipelineType.KERNEL_LEGACY))


def is_kernel_rt_pipeline(name):
    """Return True if the name matches PipelineType.KERNEL_RT or KERNEL_RT_LEGACY, else False."""
    return bool(get_pipeline_type(name) in (PipelineType.KERNEL_RT,
                                            PipelineType.KERNEL_RT_LEGACY))


def is_kernel_shadow_pipeline(name):
    """Return True if the name matches PipelineType.KERNEL_SHADOW, else False."""
    return bool(get_pipeline_type(name) is PipelineType.KERNEL_SHADOW)
