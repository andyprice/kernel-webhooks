"""Library to consistently parse an MR or commit description."""
from dataclasses import dataclass
from dataclasses import field
import re

from cki_lib.logger import get_logger

from webhook.libbz import fetch_bugs

LOGGER = get_logger('cki.webhook.description')

BUGZILLA_TAG = 'Bugzilla'
DEPENDS_TAG = 'Depends'


@dataclass
class Description():
    """Parse text tags into sets of IDs."""

    text: str = field(repr=False)
    namespace: str = ''                         # Optional namespace to match Depends: MR ID tags
    _depends: set = field(default_factory=set, init=False)  # Hack to store more Depends: BZ IDs

    def __bool__(self):
        """Return True if self.text is not empty, otherwise False."""
        return bool(self.text)

    def __eq__(self, other):
        """Return True if the text, namespace, and _depends attribute values are the same."""
        return self.text == other.text and self.namespace == other.namespace and \
            self._depends == other._depends

    @staticmethod
    def _parse_tag(tag_prefix, text):
        """Return the set of tag IDs as ints."""
        # tag_prefix: http://bugzilla.redhat.com/1234567
        # tag_prefix: https://bugzilla.redhat.com/show_bug.cgi?id=1234567
        pattern = r'^' + tag_prefix + \
            r': https?://bugzilla\.redhat\.com/(?:show_bug\.cgi\?id=)?(\d{4,8})\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return {int(tag) for tag in tag_regex.findall(text)}

    @property
    def bugzilla(self):
        """Return the set of BZ IDs parsed from any Bugzilla: tags."""
        return self._parse_tag(BUGZILLA_TAG, self.text)

    @property
    def cve(self):
        """Return the set of CVE IDs parsed from CVE: tags."""
        pattern = r'^CVE: (CVE-\d{4}-\d{4,7})\s*$'
        cve_regex = re.compile(pattern, re.MULTILINE)
        return set(cve_regex.findall(self.text))

    @property
    def depends(self):
        """Return the set of BZ IDs parsed from any Depends: tags plus any stashed in _depends."""
        return self._parse_tag(DEPENDS_TAG, self.text) | self._depends

    def add_to_depends(self, mr_ids):
        """Add the mr_id list to the _depends set that is appended to the depends set."""
        self._depends.update(mr_ids)

    @property
    def depends_mrs(self):
        """Return the set of MR IDs parsed from any Depends: tags."""
        # Depends: https://gitlab.com/group/subgroup/project/-/merge_requests/123
        # Depends: !123
        dep_mrs = set()
        if self.namespace:
            pattern = r'^' + DEPENDS_TAG + \
                r': (?:https?://gitlab\.com/' + self.namespace + r'/-/merge_requests/|!)(\d+)\s*$'
        else:
            pattern = r'^' + DEPENDS_TAG + r': !(\d+)\s*$'
        mr_regex = re.compile(pattern, re.MULTILINE)
        dep_mrs.update([int(bz_id) for bz_id in mr_regex.findall(self.text)])

        # If we have a namespace and there are Depends BZs then try to find their MR IDs.
        if self.namespace:
            if dep_bzs := self._parse_tag(DEPENDS_TAG, self.text):
                for bz_bug in fetch_bugs(dep_bzs):
                    if mr_ids := self._get_bz_mr_id(bz_bug.external_bugs, self.namespace):
                        LOGGER.info('Bug %s resolved to MR(s) %s', bz_bug.id, mr_ids)
                        dep_mrs.update(mr_ids)
                    else:
                        LOGGER.info('Bug %s not linked to any MR in the %s namespace.', bz_bug.id,
                                    self.namespace)
        return dep_mrs

    @property
    def marked_internal(self):
        """Return True if the text has Bugzilla: INTERNAL, otherwise False."""
        pattern = r'^' + BUGZILLA_TAG + r': INTERNAL\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return bool(tag_regex.findall(self.text))

    @property
    def signoff(self):
        """Return the set of valid DCO Signed-off-by tags."""
        # Each item in the set is a tuple with the name and email.
        # There is no validation of the email string.
        pattern = r'^Signed-off-by: ((?:\w+ ?)+) <(.+)>\s*$'
        dco_regex = re.compile(pattern, re.MULTILINE)
        return set(dco_regex.findall(self.text))

    @staticmethod
    def _get_bz_mr_id(external_bugs, namespace):
        """Return the list of MR IDs in the BZs external trackers that match the namespace."""
        return {int(eb['ext_bz_bug_id'].split('/')[-1]) for eb in external_bugs if
                eb['type']['url'] == 'https://gitlab.com/' and
                eb['ext_bz_bug_id'].startswith(f'{namespace}/-/merge_requests/')}
