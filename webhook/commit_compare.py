"""Query MRs for upstream commit IDs to compare against submitted patches."""
from dataclasses import dataclass
from dataclasses import field
import enum
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
# GitPython
import git
from gitlab.const import MAINTAINER_ACCESS
from gitlab.exceptions import GitlabGetError

from webhook.rh_metadata import Projects

from . import cdlib
from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.commit_compare')
SESSION = session.get_session('cki.webhook.commit_compare')


def find_intentionally_omitted_fixes(mri, description):
    """Add intentionally omitted fixes hashes to mri.omitted set."""
    fixes_pattern = r'^\s*(?P<pfx>Omitted-fix:)[\s]+(?P<hash>[a-f0-9]{8,40})'
    fixes_re = re.compile(fixes_pattern, re.IGNORECASE)
    for line in description.split('\n'):
        gitref = fixes_re.match(line)
        if gitref:
            githash = gitref.group('hash')
            mri.omitted.add(githash)
    LOGGER.debug('List of %d Omitted-fixes: %s', len(mri.omitted), mri.omitted)


def map_fixes_to_commits(mri, repo):
    """Map potential missing fixes to their corresponding MR commits."""
    for ucid, fixes in mri.fixes.items():
        for rhcommit in mri.rhcommits:
            for fix in fixes:
                if ucid in rhcommit.ucids:
                    rhcommit.fixes += repo.git.log("-1", "--pretty=short", fix)
                    rhcommit.fixes += f'\n    RH-Fixes: {rhcommit.commit.id[:12]} '
                    rhcommit.fixes += f'("{rhcommit.commit.title}")\n\n'


def filter_fixes(mri, mapped_fixes):
    """Filter potential fixes against included commits and Omitted-fix refs."""
    for fixee, fixes in mapped_fixes.items():
        per_commit_fixes = []
        for fix in fixes:
            propose = True
            for ucid in mri.ucids:
                if ucid.startswith(fix):
                    LOGGER.debug("Found %s in ucids", fix)
                    propose = False
                    continue
            for omit in mri.omitted:
                if omit.startswith(fix):
                    LOGGER.debug("Found %s in omitted", fix)
                    propose = False
                    continue
            if propose:
                per_commit_fixes.append(fix)
        if per_commit_fixes:
            mri.fixes[fixee] = per_commit_fixes


def find_potential_missing_fixes(mri):
    """Store potential missing upstream fixes hashes in mri.fixes mapping."""
    fixes_pattern = r'^(?P<hash>[a-f0-9]{8,40})'
    fixes_re = re.compile(fixes_pattern, re.IGNORECASE)
    mapped_fixes = {}

    repo = git.Repo(mri.linux_src)
    for ucid in mri.ucids:
        fixes = []
        short_hash = ucid[:12]
        # don't die if the "upstream" commit isn't valid (may be from ark, y-stream, etc)
        try:
            fixeslog = repo.git.log("--oneline", f'--grep=Fixes: {short_hash}', f'{short_hash}..')
        except git.GitCommandError:
            fixeslog = ""
        for line in fixeslog.split('\n'):
            gitref = fixes_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                fixes.append(githash)
        if fixes:
            mapped_fixes[ucid] = fixes

    filter_fixes(mri, mapped_fixes)
    LOGGER.debug('Map of possible Fixes: %s', mri.fixes)
    map_fixes_to_commits(mri, repo)


def extract_ucid(mri, commit):
    """Extract upstream commit ID from the message."""
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements
    merge_title_prefixes = ["Merge: ", "Merge branch "]
    if any(commit.title.startswith(prefix) for prefix in merge_title_prefixes):
        commit_ref = mri.project.commits.get(commit.id)
        if len(commit_ref.parent_ids) > 1:
            return ["Merge"], ""
    message_lines = commit.message.split('\n')
    errors = ""
    gitref_list = []
    have_exact_match = False
    # Pattern for 'git show <commit>' (and git log) based backports
    gl_pattern = r'^\s*(?P<pfx>commit)[\s]+(?P<hash>[a-f0-9]{8,40})$'
    gitlog_re = re.compile(gl_pattern, re.IGNORECASE)
    # Pattern for 'git cherry-pick -x <commit>' based backports
    cp_pattern = r'^\s*\((?P<pfx>cherry picked from commit)[\s]+(?P<hash>[a-f0-9]{8,40})\)$'
    gitcherrypick_re = re.compile(cp_pattern, re.IGNORECASE)
    # Look for 'RHEL-only' patches
    rhelonly_re = re.compile(r'^Upstream[ -][Ss]tatus: RHEL[ -]*[0-9.Zz]*[ -][Oo]nly')
    # Look for 'Posted' patches, posted upstream but not in a git tree yet
    posted_re = re.compile('^Upstream[ -][Ss]tatus: Posted')
    # Look for 'Posted' patches that are under embargo
    embargo_re = re.compile('^Upstream[ -][Ss]tatus: Embargo')
    # Look for 'Posted' patches, which are in a git tree we don't track (yet?)
    upstream_status_re = re.compile('^Upstream[ -][Ss]tatus: ')
    tree_prefixes = ('git://anongit.freedesktop.org/',
                     'https://anongit.freedesktop.org/git/',
                     'https://git.kernel.org/pub/scm/',
                     'git://git.kernel.org/pub/scm/',
                     'git://git.infradead.org/',
                     'http://git.linux-nfs.org/',
                     'git://linux-nfs.org/',
                     'https://github.com/',
                     'https://git.samba.org/')

    find_intentionally_omitted_fixes(mri, commit.message)
    for line in message_lines:
        gitref = gitlog_re.match(line)
        if gitref:
            githash = gitref.group('hash')
            hash_prefix = gitref.group('pfx')
            if line == f"{hash_prefix} {githash}" and len(githash) == 40:
                if githash not in gitref_list:
                    gitref_list.append(githash)
                mri.ucids.add(githash)
                have_exact_match = True
            else:
                errors += f"\n* Incorrect format git commit line in {commit.short_id}.  \n"
            if have_exact_match and errors:
                errors = ""
                continue
            if githash not in gitref_list:
                gitref_list.append(githash)
            mri.ucids.add(githash)
            if line != f"{hash_prefix} {githash}":
                errors += f"`Expected:` `{hash_prefix} {githash}`  \n"
                errors += f"`Found   :` `{line}`  \n"
            if len(githash) < 40:
                errors += f"Git hash only has {len(githash)} characters, expected 40.  \n"
            continue
        gitref = gitcherrypick_re.match(line)
        if gitref:
            githash = gitref.group('hash')
            hash_prefix = gitref.group('pfx')
            if line == f"({hash_prefix} {githash})" and len(githash) == 40:
                if githash not in gitref_list:
                    gitref_list.append(githash)
                mri.ucids.add(githash)
                have_exact_match = True
            else:
                errors += f"\n* Incorrect format git cherry-pick line in {commit.short_id}.  \n"
            if have_exact_match and errors:
                errors = ""
                continue
            if githash not in gitref_list:
                gitref_list.append(githash)
            mri.ucids.add(githash)
            if line != f"({hash_prefix} {githash})":
                errors += f"`Expected:` `({hash_prefix} {githash})`  \n"
                errors += f"`Found   :` `{line}`  \n"
            if len(githash) < 40:
                errors += f"Git hash only has {len(githash)} characters, expected 40.  \n"
            continue
        if rhelonly_re.findall(line):
            gitref_list.append("RHELonly")
            continue
        if posted_re.findall(line):
            gitref_list.append("Posted")
            continue
        if embargo_re.findall(line):
            gitref_list.append("Posted")
            continue
        if upstream_status_re.findall(line):
            for prefix in tree_prefixes:
                if prefix in line:
                    LOGGER.debug("Found tree_prefix in: %s", line)
                    gitref_list.append("Posted")
    if "RHELonly" in gitref_list:
        LOGGER.debug("This commit is marked as RHELonly, ignore all other found commit refs")
        gitref_list = ["RHELonly"]
        errors = ""
    # We return empty arrays if no commit IDs are found
    LOGGER.debug("Found upstream refs: %s", gitref_list)
    if len(gitref_list) > 5:
        LOGGER.info("Excessive number of commits found: %d, call it RHEL-only", len(gitref_list))
        gitref_list = ["RHELonly"]
    if errors != "":
        LOGGER.warning("Upstream commit reference errors for submitted commit %s:\n%s",
                       commit.short_id, errors)
    return gitref_list, errors


def noucid_msg():
    """Return the message to use as a note for commits without an upstream commit ID."""
    msg = "No commit information found.  All commits are required to have a 'commit: "
    msg += "<upstream_commit_hash>' entry, or an 'Upstream Status: RHEL-only' entry "
    msg += "with an explanation why this change is not upstream. Alternatively, an "
    msg += "'Upstream Status: Posted' entry followed by a URL pointing to the posting "
    msg += "is also acceptable.  Please review this commit and appropriate metadata.  "
    msg += "Guidelines for these entries can be found in CommitRules,"
    msg += "https://red.ht/kwf_commit_rules."
    return msg


def posted_msg():
    """Return the message to use as note for commits that claim to be Posted."""
    msg = "This commit has Upstream Status as Posted, but we're not able to auto-compare it.  "
    msg += "Reviewers should take additional care when reviewing these commits."
    return msg


def merge_msg():
    """Return the message to use as note for commits that appear to be merge commits."""
    msg = "This commit looks like a merge commit, probably for dependencies in the series.  "
    msg += "Reviewers should evaluate these commits accordingly."
    return msg


def rhelonly_msg():
    """Return the message to use as note for commits that claim to be RHEL-only."""
    msg = "This commit has Upstream Status as RHEL-only and has no corresponding upstream commit.  "
    msg += "Reviewers should take additional care when reviewing these commits."
    return msg


def unk_cid_msg():
    """Return the message to use as note for commits that have an unknown commit ID."""
    msg = "No upstream source commit found.  This commit references an upstream commit ID, but "
    msg += "its source of origin is not recognized.  Please verify the upstream source tree."
    return msg


def diffs_msg():
    """Return the message to use as note for commits that differ from upstream."""
    msg = "This commit differs from the referenced upstream commit and should be evaluated "
    msg += "accordingly."
    return msg


def kabi_msg():
    """Return the message to use as note for commits that may contain kABI work-arounds."""
    msg = "This commit references a kABI work-around, and should be evaluated accordingly."
    return msg


def partial_msg():
    """Return the message to use as not for commits that match the part submitted."""
    msg = "This commit is a partial backport of the referenced upstream commit ID, and "
    msg += "the portions backported match upstream 100%."
    return msg


def badmail_msg():
    """Return the message to use for commits with invalid authorship."""
    msg = "This commit has invalid authorship. Commits must either have an authorship "
    msg += "email in the redhat.com domain, or they must match the email address of the "
    msg += "submitter of the merge request for outside contributors."
    return msg


class Match(enum.IntEnum):
    """Match versus upstream commit's diff."""

    NOUCID = 0
    FULL = 1
    PARTIAL = 2
    DIFFS = 3
    KABI = 4
    RHELONLY = 5
    POSTED = 6
    BADMAIL = 7
    MERGECOMMIT = 8


def get_report_table(mri):
    """Create the table for the upstream commit ID report."""
    table = []
    for rhcommit in mri.rhcommits:
        table.append([rhcommit.commit.id, rhcommit.ucids, rhcommit.match, rhcommit.notes, ""])
    return table


def get_match_info(match):
    """Get info on match type."""
    mstr = "No UCID   "
    if match == Match.FULL:
        mstr = "100% match"
    elif match == Match.PARTIAL:
        mstr = "Partial   "
    elif match == Match.DIFFS:
        mstr = "Diffs     "
    elif match == Match.KABI:
        mstr = "kABI Diffs"
    elif match == Match.RHELONLY:
        mstr = "n/a       "
    elif match == Match.POSTED:
        mstr = "n/a       "
    elif match == Match.BADMAIL:
        mstr = "Bad email "
    elif match == Match.MERGECOMMIT:
        mstr = "Merge commit"
    return mstr


def has_upstream_commit_hash(entry):
    """Figure out if this patch has an upstream commit hash or not."""
    if entry == Match.NOUCID:
        return False
    if entry == Match.RHELONLY:
        return False
    if entry == Match.POSTED:
        return False
    if entry == Match.MERGECOMMIT:
        return False
    return True


def build_fixes_comment(mri):
    """Build a comment for possible missing Fixes commits."""
    fixes_msg = ""
    if len(mri.fixes) > 0:
        fixes_msg += "\nPossible missing Fixes detected upstream:  \n"
        fixes_msg += "```\n"
        for rhcommit in mri.rhcommits:
            fixes_msg += rhcommit.fixes
        fixes_msg += "```\n"
    return fixes_msg


def print_gitlab_report(mri, table):
    """Print an upstream commit ID mapping report for gitlab."""
    # pylint: disable=too-many-locals,too-many-branches
    kerneloscope = "http://kerneloscope.usersys.redhat.com"
    show_full_match_note = False
    evaluated = len(mri.rhcommits)
    pnum = 0
    report = "<br>\n\n**Upstream Commit ID Readiness "
    if mri.approved and mri.status == defs.READY_SUFFIX:
        report += "Report**\n\n"
    else:
        report += "Error(s)!**\n\n"
    report += "This report indicates how backported commits compare to the upstream source " \
              "commit.  Matching (or not matching) is not a guarantee of correctness.  KABI, " \
              "missing or un-backportable dependencies, or existing RHEL differences against " \
              "upstream may lead to a difference in commits. As always, care should be taken " \
              "in the review to ensure code correctness.\n"
    table_header = "|P num   |Sub CID |UCIDs   |Match     |Notes   |\n" \
                   "|:-------|:-------|:-------|:---------|:-------|\n"
    table_entries = ""
    for (rhcid, ucids, match, note, _) in reversed(table):
        pnum += 1
        if match == Match.FULL:
            show_full_match_note = True
            continue
        table_entries += f"|{str(pnum)}|{rhcid}|"
        if has_upstream_commit_hash(match):
            for ucid in ucids:
                if ucid in ('-', '(...)', 'Posted') and len(ucids) > 1:
                    continue
                table_entries += f"[{ucid[:8]}]({kerneloscope}/commit/{ucid})<br>"
        else:
            for ucid in ucids:
                table_entries += f"{ucid[:8]}<br>"
        table_entries += f"|{get_match_info(match)}|"
        table_entries += common.build_note_string(note)

    footnotes = common.print_notes(mri.notes)
    report += common.wrap_comment_table(table_header, table_entries, footnotes, "table")
    report += f"\n\nTotal number of commits analyzed: **{evaluated}**<br>"
    if mri.commit_count > evaluated:
        report += f"* Skipped dependency commits: **{mri.commit_count - evaluated}**<br>"
    if table_entries and show_full_match_note:
        report += "* Patches that match upstream 100% not shown in table<br>"
    if table_entries and mri.approved:
        report += "* Please note and evaluate differences from upstream in the table.  \n"
    if mri.approved:
        report += "\nMerge Request **passes** commit ID validation, references all present.  \n"
    else:
        report += "\nThis Merge Request contains commits that are missing upstream commit ID " \
                  "references. Please review the table. \n" \
                  " \n" \
                  "To request re-evalution after resolving any issues with the commits in the " \
                  "merge request, add a comment to this MR with only the text:  " \
                  "request-commit-id-evaluation \n"
    report += build_fixes_comment(mri)

    return report


def print_text_report(mri, table):
    """Print an upstream commit ID mapping report for the text console."""
    # pylint: disable=too-many-locals
    show_full_match_note = False
    evaluated = len(mri.rhcommits)
    pnum = 0
    report = "\n\nUpstream Commit ID Readiness Report\n\n"
    table_header = "|P num   |Sub CID |UCIDs   |Match     |Notes   |\n"
    table_header += "|:-------|:-------|:-------|:---------|:-------|\n"
    table_entries = ""
    for (rhcid, ucids, match, note, logs) in reversed(table):
        pnum += 1
        if match == Match.FULL:
            show_full_match_note = True
            continue
        commits = []
        for ucid in ucids:
            if ucid not in ('-', 'Posted') or len(ucids) == 1:
                commits.append(ucid)
        commits = common.build_commits_for_row((rhcid, commits, match, note, logs))
        table_entries += f"|{str(pnum)}|{str(rhcid[:8])}"
        table_entries += f"|{' '.join(commits)}"
        table_entries += f"|{get_match_info(match)}|{', '.join(note)}|\n"
    if table_entries:
        report += table_header + table_entries
    else:
        report += "Patches all match upstream 100%\n\n"
    report += common.print_notes(mri.notes)
    report += f"Total number of commits analyzed: **{evaluated}**\n"
    if mri.commit_count > evaluated:
        report += f"* Skipped dependency commits: **{mri.commit_count - evaluated}**\n"
    if table_entries and mri.approved:
        report += "Please verify differences from upstream.\n"
    if table_entries and show_full_match_note:
        report += "* Patches that match upstream 100% not shown in table\n"
    status = "passes" if mri.approved and mri.status == defs.READY_SUFFIX else "fails"
    report += f"\nMerge Request {status} upstream commit ID validation.\n"
    return report


def get_upstream_diff(mri, ucid, filelist):
    """Extract diff for upstream commit ID, optionally limiting to filelist."""
    repo = git.Repo(mri.linux_src)

    try:
        commit = repo.commit(ucid)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.debug("Commit ID %s not found in upstream", ucid)
        return None, ""

    diff = repo.git.diff(f"{commit}^", commit)
    # author_name = commit.author.name
    author_email = commit.author.email

    if len(filelist) == 0:
        return diff, author_email

    LOGGER.debug("Getting partial diff for %s", ucid)
    part_diff = cdlib.get_partial_diff(diff, filelist)

    return part_diff, author_email


def find_kabi_hints(message, diff):
    """Check for hints this patch has kABI workarounds in it."""
    kabi_re = re.compile(r'kabi(?![a-zA-Z])', re.IGNORECASE)
    if kabi_re.findall(message):
        return True
    if kabi_re.findall(diff):
        return True
    for keyword in ("genksyms", "rh_reserved"):
        if keyword in message.lower():
            return True
        if keyword in diff.lower():
            return True
    return False


def process_no_ucid_commit(mri, rhcommit):
    """Process a commit w/no associated upstream commit ID."""
    mri.notes += [] if mri.nids['noucid'] else [noucid_msg()]
    mri.nids['noucid'] = mri.nids['noucid'] if mri.nids['noucid'] else str(len(mri.notes))
    rhcommit.match = Match.NOUCID
    rhcommit.notes.append(mri.nids['noucid'])


def process_rhel_only_commit(mri, rhcommit):
    """Process a commit listed as being RHEL-only."""
    mri.notes += [] if mri.nids['rhelonly'] else [rhelonly_msg()]
    mri.nids['rhelonly'] = mri.nids['rhelonly'] if mri.nids['rhelonly'] else str(len(mri.notes))
    rhcommit.match = Match.RHELONLY
    rhcommit.notes.append(mri.nids['rhelonly'])


def process_posted_commit(mri, rhcommit):
    """Process a commit listed as being Posted upstream."""
    mri.notes += [] if mri.nids['posted'] else [posted_msg()]
    mri.nids['posted'] = mri.nids['posted'] if mri.nids['posted'] else str(len(mri.notes))
    rhcommit.match = Match.POSTED
    rhcommit.notes.append(mri.nids['posted'])


def process_merge_commit(mri, rhcommit):
    """Process a commit that is a merge commit (probably for dependencies)."""
    mri.notes += [] if mri.nids['merge'] else [merge_msg()]
    mri.nids['merge'] = mri.nids['merge'] if mri.nids['merge'] else str(len(mri.notes))
    rhcommit.match = Match.MERGECOMMIT
    rhcommit.notes.append(mri.nids['merge'])


def process_unknown_commit(mri, rhcommit):
    """Process a commit with an unrecognized commit ID ref."""
    mri.notes += [] if mri.nids['unknown'] else [unk_cid_msg()]
    mri.nids['unknown'] = mri.nids['unknown'] if mri.nids['unknown'] else str(len(mri.notes))
    rhcommit.match = Match.NOUCID
    rhcommit.notes.append(mri.nids['unknown'])


def process_partial_backport(mri, rhcommit):
    """Process a commit that is a partial backport of an upstream commit."""
    mri.notes += [] if mri.nids['part'] else [partial_msg()]
    mri.nids['part'] = mri.nids['part'] if mri.nids['part'] else str(len(mri.notes))
    rhcommit.match = Match.PARTIAL
    rhcommit.notes.append(mri.nids['part'])


def process_kabi_patch(mri, rhcommit):
    """Process a commit that references kABI workarounds."""
    mri.notes += [] if mri.nids['kabi'] else [kabi_msg()]
    mri.nids['kabi'] = mri.nids['kabi'] if mri.nids['kabi'] else str(len(mri.notes))
    rhcommit.match = Match.KABI
    rhcommit.notes.append(mri.nids['kabi'])


def process_commit_with_diffs(mri, rhcommit):
    """Process a commit with differences from upstream."""
    mri.notes += [] if mri.nids['diffs'] else [diffs_msg()]
    mri.nids['diffs'] = mri.nids['diffs'] if mri.nids['diffs'] else str(len(mri.notes))
    rhcommit.match = Match.DIFFS
    rhcommit.notes.append(mri.nids['diffs'])


def process_invalid_author_commit(mri, rhcommit):
    """Process a commit with invalid authorship."""
    mri.notes += [] if mri.nids['badmail'] else [badmail_msg()]
    mri.nids['badmail'] = mri.nids['badmail'] if mri.nids['badmail'] else str(len(mri.notes))
    rhcommit.match = Match.BADMAIL
    rhcommit.notes.append(mri.nids['badmail'])


def valid_rhcommit_author(mri, rhcommit):
    """Attempt to make sure we have proper authorship on submitted commits."""
    # if the commit has a redhat.com author, it's valid
    rhcommit_email = rhcommit.commit.author_email
    rhcommit_email_parts = rhcommit.commit.author_email.split('@')
    if len(rhcommit_email_parts) != 2:
        LOGGER.info("Committer email address (%s) not valid", rhcommit_email)
        return False

    if rhcommit_email_parts[1] == 'redhat.com':
        return True

    if common.match_gl_username_to_email(mri.lab, rhcommit_email, mri.submitter_handle):
        LOGGER.debug("Committer and MR submitter username match @%s", mri.submitter_handle)
        return True

    # anything else... let's flag it invalid.
    LOGGER.warning("Committer email %s does not map to MR submitter username @%s",
                   rhcommit_email, mri.submitter_handle)
    return False


def no_upstream_commit_data(mri, rhcommit):
    """Handle a commit that has no upstream commit ID data."""
    if "-" in rhcommit.ucids:
        return process_no_ucid_commit(mri, rhcommit)

    if "RHELonly" in rhcommit.ucids:
        return process_rhel_only_commit(mri, rhcommit)

    if "Posted" in rhcommit.ucids:
        return process_posted_commit(mri, rhcommit)

    return None


def validate_commit_ids(mri):
    """Iterate through the upstream commit IDs we found and compare w/our submitted commits."""
    # pylint: disable=too-many-branches
    for rhcommit in mri.rhcommits:
        commit = rhcommit.commit
        no_ucid_invalid = True

        if "Merge" in rhcommit.ucids:
            process_merge_commit(mri, rhcommit)
            continue

        if any(ucid in ["-", "RHELonly", "Posted"] for ucid in rhcommit.ucids):
            no_upstream_commit_data(mri, rhcommit)
            mydiff, _ = cdlib.get_submitted_diff(commit.diff(per_page=100))
            if find_kabi_hints(commit.message, mydiff):
                process_kabi_patch(mri, rhcommit)
            no_ucid_invalid = False

        if not valid_rhcommit_author(mri, rhcommit):
            process_invalid_author_commit(mri, rhcommit)
            continue

        found_a_patch = False
        for ucid in rhcommit.ucids:
            if ucid in ("-", "RHELonly", "Posted"):
                continue
            udiff, _ = get_upstream_diff(mri, ucid, [])
            if udiff is None:
                if no_ucid_invalid and not found_a_patch:
                    process_unknown_commit(mri, rhcommit)
                continue
            found_a_patch = True
            mydiff, filelist = cdlib.get_submitted_diff(commit.diff(per_page=100))
            idiff = cdlib.compare_commits(udiff, mydiff)

            if len(idiff) == 0:
                rhcommit.match = Match.FULL
            else:
                diff = "```diff\n"
                diff += f"--- Upstream {ucid}\n"
                diff += f"+++ Backport {commit.id}\n"
                for entry in idiff[2:]:
                    diff += f"{entry}\n"
                LOGGER.debug("Patch diff:\n%s", diff)

                udiff_partial, _ = get_upstream_diff(mri, ucid, filelist)
                idiff = cdlib.compare_commits(udiff_partial, mydiff)
                if len(idiff) == 0:
                    process_partial_backport(mri, rhcommit)
                else:
                    process_commit_with_diffs(mri, rhcommit)
                    if find_kabi_hints(commit.message, mydiff):
                        process_kabi_patch(mri, rhcommit)


def add_kabi_label(mri, table):
    """Add a kabi label if the MR containts a commit flagged as kabi-related."""
    for row in table:
        if row[2] == Match.KABI:
            common.add_label_to_merge_request(mri.lab, mri.project, mri.merge_request.iid, ['KABI'])
            return True
    return False


def run_zstream_comparison_checks(mri):
    """Do some rudimentary validation of z-stream backports vs. their y-stream counterparts."""
    # pylint: disable=too-many-locals,too-many-branches
    target = str(mri.merge_request.target_branch)
    if target in ("main", "main-rt", "main-automotive", "os-build"):
        return ""

    branch_data = Projects().get_project_by_id(mri.project.id).get_branch_by_name(target)
    if not branch_data.zstream_target_release:
        return ""

    ycommit_pattern = r'^Y-Commit: (?P<hash>[a-f0-9]{8,40})$'
    ycommit_re = re.compile(ycommit_pattern, re.IGNORECASE)
    yrefs = {}
    zcompare_notes = ""

    (has_deps, dep_sha) = cdlib.get_dependencies_data(mri.merge_request)
    for commit in mri.merge_request.commits():
        # Exit the loop once we hit the first dependency commit
        if has_deps and cdlib.is_first_dep(commit, dep_sha):
            break
        cmsg_lines = commit.message.split('\n')
        ycommit_found = False
        for line in cmsg_lines:
            gitref = ycommit_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                yrefs[githash] = commit
                ycommit_found = True
                break
        if not ycommit_found:
            LOGGER.debug("Z-Commit %s has no Y-Commit reference", commit.id)
            if mri.authlevel >= MAINTAINER_ACCESS:
                zcompare_notes += f"Z-Commit {commit.id} has no Y-Commit reference  \n"

    for ycid, zcid in yrefs.items():
        LOGGER.debug("Comparing Y-Commit: %s to Z-Commit: %s", ycid, zcid.id)
        zdiff, _ = cdlib.get_submitted_diff(zcid.diff(per_page=100))
        try:
            ycommit = mri.project.commits.get(ycid)
        except GitlabGetError:
            zcompare_notes += f"Y-Commit {ycid} listed for Z-Commit {zcid.id} is invalid  \n"
            continue
        ydiff, _ = cdlib.get_submitted_diff(ycommit.diff(per_page=100))
        idiff = cdlib.compare_commits(ydiff, zdiff, strict_header=True)
        if idiff:
            LOGGER.debug("Y-Commit %s and Z-Commit %s do not match", ycid, zcid.id)
            header = f"Y-Commit {ycommit.id} and Z-Commit {zcid.id} do not match:  \n"
            desc = "interdiff"
            diff = "```diff\n"
            diff += f"--- Y-Commit {ycommit.id}\n"
            diff += f"+++ Z-Commit {zcid.id}\n"
            for entry in idiff[2:]:
                diff += f"{entry}\n"
            zcompare_notes += common.wrap_comment_table(header, diff, "", desc)
        else:
            LOGGER.debug("Y-Commit %s and Z-Commit %s match perfectly", ycommit.id, zcid.id)
            zcompare_notes += f"Y-Commit {ycommit.id} and Z-Commit {zcid.id} match 100%  \n"

    return zcompare_notes


def check_on_ucids(mri):
    """Check upstream commit IDs."""
    error_notes = ""

    # do NOT run validation if the commit count is over 2000
    if mri.commit_count > defs.MAX_COMMITS_PER_MR and mri.authlevel < MAINTAINER_ACCESS:
        error_notes = "**Upstream Commit ID Readiness Error(s)**  \n"
        error_notes += "*ERROR*: This Merge Request has too many commits for the commit reference "
        error_notes += f"webhook to process ({mri.commit_count}) -- please make sure you have "
        error_notes += "targeted the correct branch."
        return [], error_notes

    dep_label = cdlib.set_dependencies_label(mri.lab, mri.project, mri.merge_request)
    # re-fetch MR to get updated Dependencies label, if needed
    if dep_label not in mri.merge_request.labels:
        mri.merge_request = mri.project.mergerequests.get(mri.merge_request.iid)
    (has_deps, dep_sha) = cdlib.get_dependencies_data(mri.merge_request)
    for commit in mri.merge_request.commits():
        # Exit the loop once we hit the first dependency commit
        if has_deps and cdlib.is_first_dep(commit, dep_sha):
            break
        mri.rhcommits.append(RHCommit(commit))

    for rhcommit in mri.rhcommits:
        try:
            found_refs, errors = extract_ucid(mri, rhcommit.commit)
        # pylint: disable=broad-except
        except Exception:
            found_refs = []
            errors = ""

        error_notes += errors

        if not found_refs and cdlib.is_rhdocs_commit(rhcommit.commit):
            found_refs = ["RHELonly"]

        rhcommit.ucids = found_refs

    LOGGER.debug('List of %d ucids: %s', len(mri.ucids), mri.ucids)
    validate_commit_ids(mri)
    table = get_report_table(mri)
    desc = "error details"
    error_notes = common.wrap_comment_table("", error_notes, "", desc)
    return table, error_notes


@dataclass
class RHCommit:
    """Per-MR-commit data class for storing comparison data."""

    commit: set
    ucids: set = field(default_factory=set, init=False)
    match: int = Match.NOUCID
    notes: list = field(default_factory=list)
    fixes: str = ""


# pylint: disable=too-many-instance-attributes,too-few-public-methods
class MRUCIDInstance:
    """Merge request Instance."""

    def __init__(self, lab, project, merge_request, payload):
        """Initialize the commit ID comparison instance."""
        self.linux_src = "/usr/src/linux"
        self.lab = lab
        self.log_ok_scope = False
        self.approved = True
        self.status = defs.READY_SUFFIX
        self.project = project
        self.merge_request = merge_request
        self.commit_count = 0
        self.authlevel = 0
        self.submitter_handle = ""
        self.payload = payload
        self.rhcommits = []
        self.notes = []
        self.nids = {'noucid': 0, 'rhelonly': 0, 'posted': 0, 'merge': 0, 'unknown': 0,
                     'diffs': 0, 'kabi': 0, 'part': 0, 'badmail': 0}
        self.ucids = set()
        self.omitted = set()
        self.fixes = {}

    def check_commit_ref_matches(self):
        """Look at the match status of all commits to determine CommitRefs label scope."""
        for rhcommit in self.rhcommits:
            self.approved = self.approved and rhcommit.match not in (Match.NOUCID, Match.BADMAIL)

    def report_results(self, table, errors, zcompare_notes):
        """Report the results of our examination."""
        # pylint: disable=too-many-branches
        if misc.is_production():
            report = print_gitlab_report(self, table)
            report += errors + zcompare_notes
            common.update_webhook_comment(self.merge_request, self.lab.user.username,
                                          "Upstream Commit ID Readiness", report)
        else:
            LOGGER.info('Skipping adding report in non-production')
            report = print_text_report(self, table)
            fixes_msg = build_fixes_comment(self)
            report += errors + zcompare_notes + fixes_msg
            LOGGER.info(report)
        if not self.approved:
            LOGGER.info("Some commits in MR %s require further manual inspection!",
                        self.merge_request.iid)

    def run_ucid_validation(self):
        """Do the thing, check submitted patches vs. referenced upstream commit IDs."""
        hook_name = "commit validation"
        run_on_drafts = True
        if common.do_not_run_hook(self.project, self.merge_request, hook_name, run_on_drafts):
            return

        LOGGER.info("Running upstream commit ID validation on MR %s", self.merge_request.iid)
        LOGGER.debug("Merge request description:\n%s", self.merge_request.description)
        count, authlevel = common.get_commits_count(self.project, self.merge_request)
        find_intentionally_omitted_fixes(self, self.merge_request.description)
        self.commit_count = count
        self.authlevel = authlevel
        (table, errors) = check_on_ucids(self)
        self.check_commit_ref_matches()
        # append quick label "Commitrefs" with scope "OK", "Missing" or "NeedsReview"
        if errors != "":
            self.status = defs.NEEDS_REVIEW_SUFFIX
        if not self.approved:
            self.status = defs.MISSING_SUFFIX
        common.add_label_to_merge_request(self.lab, self.project, self.merge_request.iid,
                                          [f'CommitRefs::{self.status}'])

        if self.commit_count > defs.MAX_COMMITS_PER_MR and authlevel < MAINTAINER_ACCESS:
            common.update_webhook_comment(self.merge_request, self.lab.user.username,
                                          "Upstream Commit ID Readiness", errors)
            return

        # Don't run on massive MRs, it's ... not fast
        if self.commit_count <= defs.MAX_COMMITS_PER_MR:
            find_potential_missing_fixes(self)

        zcompare_notes = run_zstream_comparison_checks(self)
        if zcompare_notes:
            zcompare_notes = "\n\nZ-stream comparison report:  \n---\n" + zcompare_notes

        # append quick label "KABI" if we flagged any commit as possibly impacting kabi
        if add_kabi_label(self, table):
            LOGGER.debug("This MR (%s) impacts kABI", self.merge_request.iid)
        self.report_results(table, errors, zcompare_notes)


def get_mri(gl_instance, message, key):
    """Return a merge request instance for the webhook payload."""
    gl_project = gl_instance.projects.get(message.payload["project"]["path_with_namespace"])
    if not (gl_mergerequest := common.get_mr(gl_project, message.payload[key]["iid"])):
        return None
    return MRUCIDInstance(gl_instance, gl_project, gl_mergerequest, message.payload)


def perform_mri_tasks(mri, linux_src, label_changed):
    """Perform tasks if mri is valid."""
    if mri:
        mri.submitter_handle = mri.merge_request.author['username']
        mri.linux_src = linux_src
        mri.log_ok_scope = label_changed
        mri.run_ucid_validation()


def process_mr(gl_instance, message, linux_src, **_):
    """Process a merge request message."""
    label_changed = common.has_label_prefix_changed(message.payload['changes'], 'CommitRefs::')
    if not common.mr_action_affects_commits(message) and not label_changed:
        return

    mri = get_mri(gl_instance, message, "object_attributes")
    perform_mri_tasks(mri, linux_src, label_changed)


def process_note(gl_instance, message, linux_src, **_):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    mri = get_mri(gl_instance, message, "merge_request")
    if not common.force_webhook_evaluation(message.payload['object_attributes']['note'],
                                           'commit-id'):
        return

    perform_mri_tasks(mri, linux_src, True)


WEBHOOKS = {
    'merge_request': process_mr,
    'note': process_note,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('COMMIT_COMPARE')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    args = parser.parse_args(args)
    if not args.linux_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    common.generic_loop(args, WEBHOOKS, linux_src=args.linux_src)


if __name__ == "__main__":
    main(sys.argv[1:])
