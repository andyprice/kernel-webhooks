"""Add kernel subsystem topics as labels to an MR."""
import os
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session

from . import cdlib
from . import common
from . import defs
from . import owners

LOGGER = logger.get_logger('cki.webhook.subsystems')
SESSION = session.get_session('cki.webhook.subsystems')


def path_matches(search_key, map_path_list, regex=False):
    # pylint: disable=too-many-return-statements
    """Return True if the key matches any of the path_list items."""
    if not map_path_list:
        return False
    # N: Files and directories *Regex* patterns.
    #  N:   [^a-z]tegra     all files whose path contains tegra
    #                       (not including files like integrator)
    if regex:
        for pattern in map_path_list:
            if re.search(pattern, search_key):
                return True
        return False

    # F: Files and directories with wildcard patterns.
    #  A trailing slash includes all files and subdirectory files.
    #  F:	drivers/net/	all files in and below drivers/net
    #  F:	drivers/net/*	all files in drivers/net, but not below
    #  F:	*/net/*		all files in "any top level directory"/net
    for map_path in map_path_list:
        if owners.glob_match(search_key, map_path):
            return True
    return False


def make_labels(topics):
    """Given a list of topics return a list of gitlab label objects."""
    label_list = []
    for topic in topics:
        if topic.endswith(defs.NEEDS_TESTING_SUFFIX):
            label_list.append(topic)
        else:
            label_list.append(f'{defs.SUBSYS_LABEL_PREFIX}:{topic}')
    return label_list


def user_wants_notification(user_data, path_list, target_branch):
    """Determine if the given user wants notification about any of the given files."""
    for path in path_list:
        if 'all' in user_data and path_matches(path, user_data['all']):
            return True
        if target_branch in user_data and path_matches(path, user_data[target_branch]):
            return True
    return False


def get_current_subsystems_from_labels(gl_mergerequest):
    """Get the current subsystems from existing labels."""
    # One-liner equivalent of:
    # subsys_list = []
    # for label in gl_mergerequest.labels:
    #     if label.startswith(defs.SUBSYS_LABEL_PREFIX):
    #         subsystem = label.split(":")[1]
    #         subsys_list.append(subsystem)
    # return subsys_list
    return [x.split(':')[1] for x in gl_mergerequest.labels
            if x.startswith(defs.SUBSYS_LABEL_PREFIX)]


def get_stale_subsystems(old_subsystems, new_subsystems):
    """Get list of subsystems that should be removed from the ss labels."""
    return [x for x in old_subsystems if x not in new_subsystems]


def remove_stale_subsystem_labels(gl_project, mr_id, stale_subsystems):
    """Remove stale subsystem labels from the merge request."""
    remove = [f'{defs.SUBSYS_LABEL_PREFIX}:{x}' for x in stale_subsystems]
    common.remove_labels_from_merge_request(gl_project, mr_id, remove)


def set_blocking_test_labels(gl_mergerequest, blocking_test_labels):
    """Return a list of blocking test label names, if any."""
    labels = []
    LOGGER.debug('Blocking test labels: %s', blocking_test_labels)
    if misc.is_production() and gl_mergerequest.draft:
        LOGGER.info("Not adding blocking test label to MR %s in draft state", gl_mergerequest.iid)
        return labels

    if not blocking_test_labels:
        LOGGER.info('No blocking test labels to add.')
        return labels

    mr_label = None
    mr_labels = gl_mergerequest.labels
    for label in blocking_test_labels:
        blocking_label_exists = False
        for mr_label in mr_labels:
            if label[0] in mr_label:
                blocking_label_exists = True
                break
        if not blocking_label_exists:
            label = f'{label[0]}::{defs.NEEDS_TESTING_SUFFIX}'
            labels.extend(make_labels([f'{label}']))
        else:
            LOGGER.info('A blocking test label already exists (%s).', mr_label)
    return labels


def do_usermapping(target_branch, path_list, repo_path):
    """For the given branch and list of paths return a list of users to be notified."""
    # Every file in the repo 'users' directory should be the name of a GL user.
    users_path = os.path.join(repo_path, 'users')
    try:
        path_listing = os.listdir(users_path)
    except OSError:
        LOGGER.exception("Problem listing path: '%s'", users_path)
        return []

    user_list = []
    for username in path_listing:
        user_path = os.path.join(users_path, username)
        user_data = common.load_yaml_data(user_path)
        if not user_data:
            LOGGER.error("Error loading user data from path '%s'.", user_path)
            continue
        if user_wants_notification(user_data, path_list, target_branch):
            user_list.append(username)
    return user_list


def post_notifications(gl_instance, merge_request, user_list, kernel_watch_url):
    """Post a note to the MR notifying the users in user_list."""
    participants = []
    for participant in merge_request.participants():
        participants.append(participant['username'])

    new_users = ['@' + user for user in user_list if user not in participants]
    if not new_users:
        LOGGER.info('No one new to notify.')
        return

    note_text = defs.NOTIFICATION_TEMPLATE.format(header=defs.NOTIFICATION_HEADER,
                                                  users=' '.join(new_users),
                                                  project=kernel_watch_url)
    LOGGER.info('Posting notification on MR %d:\n%s', merge_request.iid, note_text)
    common.update_webhook_comment(merge_request, gl_instance.user.username,
                                  defs.NOTIFICATION_HEADER, note_text)


def _do_process_mr(gl_instance, message, mr_id, owners_parser, local_repo_path, kernel_watch_url,
                   linus_src):
    # pylint: disable=too-many-arguments,too-many-locals
    gl_project = gl_instance.projects.get(message.payload["project"]["path_with_namespace"])
    gl_mergerequest = gl_project.mergerequests.get(mr_id)
    LOGGER.info('Processing %s:%s...', gl_project.path_with_namespace, gl_mergerequest.iid)

    # first process owners_parser and assign labels ...
    path_list = cdlib.get_mr_pathlist(gl_instance, gl_project, gl_mergerequest)
    if not path_list:
        LOGGER.info("MR %s does not report any changed files, exiting.", mr_id)
        return

    # Update the path_list to include corresponding Kconfig files
    changed_config_files, config_label = common.process_config_items(linus_src, path_list)
    path_list.extend(changed_config_files)
    if config_label:
        common.add_label_to_merge_request(gl_instance, gl_project, mr_id, config_label)
    LOGGER.debug('path_list: %s', path_list)

    labels = []
    entries = owners_parser.get_matching_entries(path_list)
    LOGGER.debug('Matching owners entries: %s', entries)

    old_subsystems = get_current_subsystems_from_labels(gl_mergerequest)
    new_subsystems = [x.get_subsystem_label() for x in entries if x.get_subsystem_label()]
    stale_subsystems = get_stale_subsystems(old_subsystems, new_subsystems)
    if stale_subsystems:
        LOGGER.info('Stale subsystems (to be removed): %s', stale_subsystems)
        remove_stale_subsystem_labels(gl_project, gl_mergerequest.iid, stale_subsystems)
    LOGGER.debug('Subsystem labels: %s', new_subsystems)
    if new_subsystems:
        labels.extend(make_labels(new_subsystems))
    else:
        LOGGER.info('No labels to add.')

    if gl_project.id != defs.ARK_PROJECT_ID:
        blocking_test_labels = [x.get_ready_for_merge_label_deps()
                                for x in entries if x.get_ready_for_merge_label_deps()]
        labels.extend(set_blocking_test_labels(gl_mergerequest, blocking_test_labels))

    if labels:
        common.add_label_to_merge_request(gl_instance, gl_project, gl_mergerequest.iid, labels)

    # ... then do user notifications.
    target_branch = gl_mergerequest.target_branch \
        if gl_mergerequest.target_branch != 'main' else gl_project.namespace['name']

    user_list = do_usermapping(target_branch, path_list, local_repo_path)
    if user_list:
        post_notifications(gl_instance, gl_mergerequest, user_list, kernel_watch_url)
    else:
        LOGGER.info('No new users to notify @ for MR %d.', gl_mergerequest.iid)


def check_test_label_needs_update(payload):
    """Check for required blocking testing labels that need an update."""
    update = common.has_label_suffix_changed(payload, defs.NEEDS_TESTING_SUFFIX)
    update |= common.has_label_suffix_changed(payload, defs.TESTING_FAILED_SUFFIX)
    update |= common.has_label_suffix_changed(payload, defs.TESTING_WAIVED_SUFFIX)
    update |= common.has_label_suffix_changed(payload, defs.READY_SUFFIX)
    return update


def process_mr(gl_instance, msg, owners_parser, local_repo_path, kernel_watch_url, linus_src, **_):
    # pylint: disable=too-many-arguments
    """Process a merge request message."""
    # If the MR file contents haven't changed and our labels haven't changed then don't run.
    is_draft, draft_changed = common.draft_status(msg.payload)
    commits_changed = common.mr_action_affects_commits(msg)
    label_changed = common.has_label_prefix_changed(msg.payload['changes'],
                                                    f'{defs.SUBSYS_LABEL_PREFIX}:')
    test_label_update = check_test_label_needs_update(msg.payload)
    if not commits_changed and not label_changed and not test_label_update and not \
       (draft_changed and not is_draft):
        LOGGER.info('Event does not show any relevant changes, ignoring.')
        return
    _do_process_mr(gl_instance, msg, msg.payload["object_attributes"]["iid"], owners_parser,
                   local_repo_path, kernel_watch_url, linus_src)


WEBHOOKS = {
    "merge_request": process_mr,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SUBSYSTEMS')
    parser.add_argument('--owners-yaml', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Path to the owners.yaml file')
    parser.add_argument('--local-repo-path', **common.get_argparse_environ_opts('LOCAL_REPO_PATH'),
                        help='Local path where the kernel-watch repo is checked out')
    parser.add_argument('--kernel-watch-url',
                        **common.get_argparse_environ_opts('KERNEL_WATCH_URL'),
                        help='URL of the kernel-watch project')
    parser.add_argument('--linus-src', **common.get_argparse_environ_opts('LINUS_SRC'),
                        help='Directory where upstream will be checked out')
    args = parser.parse_args(args)

    LOGGER.info('Using owners file: %s, local repo path: %s, kernel-watch repo: %s.',
                args.owners_yaml, args.local_repo_path, args.kernel_watch_url)
    owners_parser = common.get_owners_parser(args.owners_yaml)

    common.generic_loop(args, WEBHOOKS, owners_parser=owners_parser,
                        local_repo_path=args.local_repo_path,
                        kernel_watch_url=args.kernel_watch_url,
                        linus_src=args.linus_src)


if __name__ == "__main__":
    main(sys.argv[1:])
