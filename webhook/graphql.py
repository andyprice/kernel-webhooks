"""Common helpers for graphql."""
from functools import reduce
from operator import getitem

from cki_lib.gitlab import get_token
from cki_lib.logger import get_logger
from cki_lib.misc import is_production
from cki_lib.session import get_session
from gql import Client
from gql import gql
from gql.transport.exceptions import TransportQueryError
from gql.transport.requests import RequestsHTTPTransport

from webhook import fragments

LOGGER = get_logger('cki.webhook.graphql')

GET_USER_DETAILS_BASE = """
query userData {
  ...CurrentUser
}
"""

GET_USER_DETAILS_QUERY = gql(GET_USER_DETAILS_BASE + fragments.CURRENT_USER)

# Query for GitlabGraph.create_note()
CREATE_NOTE_MUTATION = gql("""
mutation createNote($globalId: NoteableID!, $body: String!) {
  createNote(input: {noteableId: $globalId, body: $body}) {
    note {
      id
    }
  }
}
""")

UPDATE_NOTE_MUTATION = gql("""
mutation updateNote($globalId: NoteID!, $body: String!) {
  updateNote(input: {id: $globalId, body: $body}) {
    note {
      id
    }
  }
}
""")

FIND_NOTE_QUERY = gql("""
query mrData($namespace: ID!, $mr_id: String!) {
  project(fullPath: $namespace) {
    mergeRequest(iid: $mr_id) {
      id
      discussions {
        nodes {
          notes {
            nodes {
              author {
                username
              }
              body
              id
            }
          }
        }
      }
    }
  }
}
""")


def _check_user(results, check_user):
    """Return True if the query currentUser username matches the check_user."""
    return check_user == results['currentUser']['username']


def _check_keys(results, check_keys):
    """Return True if all the keys are in the given query results."""
    return check_keys <= results.keys()


def check_query_results(results, check_keys, check_user):
    """Perform some optional checks of query results."""
    # See GitlabGraph.execute_query().
    # Ignore our own messages (for bots). Query must include 'currentUser' field.
    if check_user and _check_user(results, check_user):
        LOGGER.info('Ignoring message from %s.', check_user)
        return None

    # Raise an error if not all expected keys are in the results.
    if check_keys and not _check_keys(results, check_keys):
        raise RuntimeError(f'Gitlab did not return all keys {check_keys} in {results}.')
    return results


class GitlabGraph:
    """A wrapper object for interacting with gitlab graphql."""

    def __init__(self, headers=None, session=None, fetch_schema=False, get_user=False):
        """Set up the client."""
        self._user = None
        self.client = None
        self.connect(headers=headers, session=session, fetch_schema=fetch_schema)
        if get_user:
            LOGGER.info('Logged in as %s (%s).', self.username, self.user_id)

    class _CkiRequestsHTTPTransport(RequestsHTTPTransport):
        """A RequestsHTTPTransport with a cki_lib session object used by our connect() method."""

        # pylint: disable=too-few-public-methods
        def __init__(self, *args, session=None, headers=None, **kwargs):
            super().__init__(*args, **kwargs)
            self.session = session or get_session(__name__)
            self.session.headers.update(headers or {})

        def connect(self) -> None:
            """Skip connect as we have our own session."""

        def close(self) -> None:
            """Skip close as we have our own session."""

    def connect(self, headers=None, session=None, fetch_schema=False):
        """Connect to graphql using cki_lib get_token()."""
        if not self.client:
            if not headers:
                headers = {}

            if not session and (token := get_token('https://gitlab.com')) is not None:
                headers['Authorization'] = f'Bearer {token}'

            transport = self._CkiRequestsHTTPTransport(session=session, headers=headers,
                                                       url='https://gitlab.com/api/graphql')
            self.client = Client(transport=transport, fetch_schema_from_transport=fetch_schema)
        LOGGER.info('Connected.')

    def _set_user_details(self):
        """Query the user details and store them in the _user attribute."""
        # pylint: disable=unsubscriptable-object
        self._user = self.execute_query(GET_USER_DETAILS_QUERY)['currentUser']

    @property
    def user(self):
        """Return the details of the user we are logged in as."""
        if not self._user:
            self._set_user_details()
        return self._user

    @property
    def user_id(self):
        """Return the user ID of the user we are logged in as."""
        if not self._user:
            self._set_user_details()
        return int(self._user['id'].rsplit('/')[-1])

    @property
    def username(self):
        """Return the username we are logged in as."""
        if not self._user:
            self._set_user_details()
        return self._user['username']

    def execute_query(self, query, params=None, check_keys=None, check_user=None):
        """Execute the query with the given params.

        Query may include:
        - currentUser field with username subfield to use check_user

        Parameters:
        query (gql.gql): The query to run.
        params (dict): Query parameters, if any.
        check_keys (set): Key names required to be in the query result, if any. Will raise
                          a RuntimeError if not all keys are found.
        check_user (str): Returns None if the query result currentUser['username'] matches
                          check_user, if any. Useful to see if we are running as a bot.

        Returns:
        dict:dict returned by gql.client.execute() or None if optional checks fail.
        """
        LOGGER.debug('Executing query with params: %s', params)
        try:
            results = self.client.execute(query, variable_values=params)
        except TransportQueryError as err:
            LOGGER.error('Encountered %d error(s) executing query: %s', len(err.errors),
                         query.loc.source.body)
            for count, error in enumerate(err.errors, start=1):
                LOGGER.error('%d: %s', count, error)
            raise
        return check_query_results(results, check_keys, check_user)

    def execute_paged_query(self, query, page_key, params=None, check_keys=None, check_user=None):
        # pylint: disable=too-many-arguments
        """Execute a query with fields that require pagination.

        See webhook.signoff.SIGNOFF_QUERY for example usage.

        Query must include:
        - $cursor variable used as after: value of the field with paged results
        - pageInfo subfield with hasNextPage and endCursor in the field with paged results
        - $first_pass variable used to @include all fields which are _not_ paged
        - (optional) currentUser field with username subfield to use check_user

        Parameters:
        query (gql.gql): The query to run.
        page_key (list): Path to location in query results where PageInfo will be.
        params (dict): Query parameters, if any.
        check_keys (tuple): Key names required to be in the query result, if any. Will raise
                            a RuntimeError if not all keys are found.
        check_user (str): Returns None if the query result currentUser['username'] matches
                          check_user, if any. Useful to see if we are running as a bot.

        Returns:
        dict:dict returned by gql.client.execute() with page_key['nodes'] packed or None if optional
             checks fail.
        """
        LOGGER.debug('Running paged queries with params: %s, page_key: %s', params, page_key)
        cursor = ''
        results = []  # first query is stored here and subsequent query results are appended
        while cursor is not None:
            params['cursor'] = cursor
            params['first_pass'] = not bool(cursor)
            # No need to check these after the first time
            if not params['first_pass']:
                check_keys = None
                check_user = None
            new_results = self.execute_query(query, params=params, check_keys=check_keys,
                                             check_user=check_user)
            if not new_results:
                return new_results
            # If all the expected results are not populated (ie. due to unknown MR ID) then
            # getitem() raises a TypeError. Just return what we got and give up?
            try:
                paged_data = reduce(getitem, page_key, new_results)
            except TypeError:
                LOGGER.warning('page_key %s not found in query results %s', page_key, new_results)
                return new_results

            if params['first_pass']:
                results = new_results
            else:
                # not our first pass so 'extend' existing results with new results
                reduce(getitem, page_key, results)['nodes'].extend(paged_data['nodes'])

            cursor = paged_data['pageInfo']['endCursor'] if \
                paged_data['pageInfo']['hasNextPage'] else None
        return results

    def _do_note(self, action, global_id, body):
        """Handle creating or updating a note."""
        params = {'globalId': global_id, 'body': body}
        if action == 'create':
            result = self.execute_query(CREATE_NOTE_MUTATION, params, check_keys={'createNote'})
        elif action == 'update':
            result = self.execute_query(UPDATE_NOTE_MUTATION, params, check_keys={'updateNote'})
        if not result:
            raise RuntimeError('Note mutation did not return expected results.')
        return result

    def create_note(self, global_id, body):
        """Create a note if in prod. Returns the new note's global ID on success, otherwise None."""
        LOGGER.info('Creating new note for %s:\n%s', global_id, body)
        if not is_production():
            return True
        result = self._do_note('create', global_id, body)
        return result['createNote']['note']['id']  # pylint: disable=unsubscriptable-object

    def update_note(self, global_id, body):
        """Update an existing note. Returns the new note's global ID on success, otherwise None."""
        LOGGER.info('Updating note on %s:\n%s', global_id, body)
        if not is_production():
            return True
        result = self._do_note('update', global_id, body)
        return result['updateNote']['note']['id']  # pylint: disable=unsubscriptable-object

    @staticmethod
    def _search_discussion(discussion, username, substring):
        """Find the note in the discussion by the username with matching substring, if any."""
        for note in discussion['notes']['nodes']:
            if note['author']['username'] == username and substring in note['body']:
                return note['id']
        return None

    def replace_note(self, namespace, mr_id, username, substring, body):
        # pylint: disable=too-many-arguments,unsubscriptable-object
        """
        Replace an existing note with body.

        Searches the MR discussions for a note by a matching username that contains the substring
        and if found, replaces it with body. If not, a new note is created.
        Operates on the first matching note.
        """
        params = {'namespace': namespace, 'mr_id': str(mr_id)}
        if not (result := self.execute_query(FIND_NOTE_QUERY, params, check_keys={'project'})):
            raise RuntimeError('Find note query did not return expected results.')
        note_id = None
        for discussion in result['project']['mergeRequest']['discussions']['nodes']:
            if note_id := self._search_discussion(discussion, username, substring):
                LOGGER.debug('Found existing note %s.', note_id)
                break
        if note_id:
            return self.update_note(note_id, body)
        return self.create_note(result['project']['mergeRequest']['id'], body)
