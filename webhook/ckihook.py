"""Manage the CKI labels."""
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from enum import IntEnum
from enum import auto
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.gitlab import get_variables
from gql import gql

from webhook import common
from webhook import defs
from webhook import fragments
from webhook.graphql import GitlabGraph
from webhook.pipelines import PipelineType
from webhook.pipelines import get_pipeline_type

LOGGER = logger.get_logger('cki.webhook.ckihook')


class PipelineStatus(IntEnum):
    """Possible status of a pipeline."""

    UNKNOWN = auto()
    MISSING = auto()
    FAILED = auto()
    CANCELED = auto()
    RUNNING = auto()
    PENDING = RUNNING
    CREATED = RUNNING
    OK = auto()
    SUCCESS = OK

    def title(self):
        """Return capitalized name."""
        return self.name.capitalize() if self.name != 'OK' else 'OK'

    @classmethod
    def from_str(cls, status):
        """Return the enum value that matches the given status name."""
        return next((member for name, member in cls.__members__.items()
                     if name == status.upper()), cls.UNKNOWN)


@dataclass(frozen=True)
class PipelineResult:
    """Basic pipeline details."""

    api_dict: InitVar(dict) = {}
    name: str = field(init=False, default='')
    status: PipelineStatus = field(init=False, default=PipelineStatus.UNKNOWN)
    label: str = field(init=False, default='')
    type: PipelineType = field(init=False, default=PipelineType.INVALID)
    failed_stage: str = field(init=False, default='')

    def __post_init__(self, api_dict):
        """Parse the api_dict."""
        self.__dict__['name'] = api_dict['sourceJob']['name']
        self.__dict__['type'] = get_pipeline_type(self.name)
        self.__dict__['status'] = PipelineStatus.from_str(api_dict['status'])
        if self.status is PipelineStatus.FAILED:
            self.__dict__['failed_stage'] = self._get_failed_stage(api_dict['stages']['nodes'])
        self._set_label()
        LOGGER.debug('Created %s', self)

    def _set_label(self):
        """Set the label for pipelines of a known `type`."""
        prefix = ''
        if self.type in (PipelineType.KERNEL, PipelineType.KERNEL_LEGACY):
            prefix = defs.CKI_KERNEL_PREFIX
        elif self.type in (PipelineType.KERNEL_RT, PipelineType.KERNEL_RT_LEGACY):
            prefix = defs.CKI_KERNEL_RT_PREFIX
        if not prefix:
            return
        self.__dict__['label'] = f'{prefix}::{self.status.title()}::{self.failed_stage}' if \
                                 self.failed_stage else f'{prefix}::{self.status.title()}'

    @staticmethod
    def _get_failed_stage(stages):
        """Return the name of the latest failed stage for a Pipeline that has failed."""
        return next((stage['name'] for stage in reversed(stages) for
                     job in stage['jobs']['nodes'] if job['status'].upper() == 'FAILED'), '')


PIPELINE_QUERY_CHECK_KEYS = {'currentUser', 'project'}
PIPELINE_BASE = """
query mrData($namespace: ID!, $mr_id: String!) {
  ...CurrentUser
  project(fullPath: $namespace) {
    mr: mergeRequest(iid: $mr_id) {
      headPipeline {
        id
        status
        downstream {
          nodes {
            ...CkiPipeline
          }
        }
      }
    }
  }
}
"""

PIPELINE_QUERY = gql(PIPELINE_BASE + fragments.CURRENT_USER + fragments.CKI_PIPELINE)


class FetchCKI(GitlabGraph):
    """Class to fetch an MR's downstream pipeline status."""

    def __init__(self, event_user, namespace, mr_id):
        """Store basic properties."""
        GitlabGraph.__init__(self)
        self.event_user = event_user  # user that triggered the webhook event
        self.namespace = namespace
        self.mr_id = mr_id

    def run_query(self):
        """Query the MR and its pipelines."""
        LOGGER.info('Fetching pipeline data for %s!%s.', self.namespace, self.mr_id)
        query_params = {'namespace': self.namespace,
                        'mr_id': str(self.mr_id),
                        }
        results = self.execute_query(PIPELINE_QUERY, query_params,
                                     check_keys=PIPELINE_QUERY_CHECK_KEYS,
                                     check_user=self.event_user)
        return self._validate_query_results(results)

    @staticmethod
    def _validate_query_results(results):
        """Check PIPELINE_QUERY query results and return them."""
        if not results:
            LOGGER.info('Nothing to parse.')
            return []
        # The MR value will be None if it doesn´t exist.
        if not results['project'].get('mr'):
            LOGGER.warning('Merge request does not exist, ignoring.')
            return []
        # If the headPipeline or downstream data is not available then we have nothing to do.
        if not (head_pipeline := results['project']['mr'].get('headPipeline')):
            LOGGER.warning('MR does not have headPipeline set.')
            return []
        if not head_pipeline['downstream']['nodes']:
            LOGGER.warning('headPipeline does not have any downstream nodes set.')
            return []
        return results


def map_pipeline_query_results(results):
    """Map query results into a list of PipelineResult objects."""
    if results:
        pipeline_data = results['project']['mr']['headPipeline']['downstream']['nodes']
        return list(map(lambda p: PipelineResult(api_dict=p), pipeline_data))
    return []


def parse_mr_url(url):
    """Return MR namespace and ID parsed from a gitlab url."""
    mr_id = int(url.split('/')[-1])
    namespace = url.removeprefix('https://gitlab.com/').removesuffix(f'/-/merge_requests/{mr_id}')
    return namespace, mr_id


def add_labels(namespace, mr_id, labels):
    """Set the given labels."""
    LOGGER.info('Setting labels %s', labels)
    gl_instance = get_instance('https://gitlab.com')
    gl_project = gl_instance.projects.get(namespace)
    common.add_label_to_merge_request(gl_instance, gl_project, mr_id, labels, remove_scoped=True)


def compute_new_labels(event_user, namespace, mr_id):
    """Return a new list of CKI labels for the given MR."""
    results = FetchCKI(event_user, namespace, mr_id).run_query()
    pipeline_list = map_pipeline_query_results(results)
    new_labels = [pipe.label for pipe in pipeline_list if pipe.label]
    # Return our new labels but if any are missing append ::Missing ones as well
    return new_labels + check_for_missing_labels(new_labels) if new_labels else []


def cki_label_changed(changes):
    """Return True if any CKI label changed, or False."""
    return (common.has_label_prefix_changed(changes, f'{defs.CKI_KERNEL_PREFIX}::') or
            common.has_label_prefix_changed(changes, f'{defs.CKI_KERNEL_RT_PREFIX}::'))


def check_for_missing_labels(label_list):
    """Return a list of CKI[_RT]::Missing labels if the input label list does not have them."""
    missing_labels = []
    for prefix in (defs.CKI_KERNEL_PREFIX, defs.CKI_KERNEL_RT_PREFIX):
        if not [label for label in label_list if label.startswith(f'{prefix}::')]:
            missing_labels.append(f'{prefix}::Missing')
    return missing_labels


def get_project_pipeline_var(gl_project, pipeline_id, key):
    """Return the value of the pipeline variable matching key, or None."""
    gl_pipeline = gl_project.pipelines.get(pipeline_id)
    return get_variables(gl_pipeline).get(key)


def get_pipeline_target_branch(gl_instance, project_id, pipeline_id):
    """Return the 'branch' pipeline variable value for the given project/pipeline."""
    gl_project = gl_instance.projects.get(project_id)
    return get_project_pipeline_var(gl_project, pipeline_id, 'branch')


def get_downstream_pipeline_branch(gl_instance, pipeline_data):
    """Return the 'branch' var value if found, otherwise None."""
    ds_pipeline = pipeline_data['project']['mr']['headPipeline']['downstream']['nodes'][0]
    ds_project_id = int(ds_pipeline['project']['id'].split('/')[-1])
    ds_pipeline_id = int(ds_pipeline['id'].split('/')[-1])

    if not (ds_branch := get_pipeline_target_branch(gl_instance, ds_project_id, ds_pipeline_id)):
        LOGGER.debug("Could not get 'branch' variable from downstream pipeline %s", ds_pipeline_id)
    return ds_branch


def process_possible_branch_change(event_user, namespace, mr_id, msg):
    """Confirm MR branch changed and if so, Return True and cancel/trigger pipelines."""
    LOGGER.info('Checking if MR target branch matches latest pipeline target branch.')
    # If the MR doesn't have a head pipeline then we're done here.
    if not msg.payload['object_attributes']['head_pipeline_id']:
        LOGGER.info('No head pipeline, skipping target branch check.')
        return False
    # This is the signature of an MR event when the target branch changes. Maybe?
    if 'merge_status' not in msg.payload['changes']:
        LOGGER.info(
            "'changes' dict does not have 'merge_status' key, skipping target branch check."
        )
        return False
    if pipeline_data := FetchCKI(event_user, namespace, mr_id).run_query():
        gl_instance = msg.gl_instance()
        ds_branch = get_downstream_pipeline_branch(gl_instance, pipeline_data)
        mr_branch = msg.payload['object_attributes']['target_branch']
        if ds_branch and ds_branch != mr_branch:
            LOGGER.info('Target branch changed: MR %s != downstream pipeline %s, triggering...',
                        mr_branch, ds_branch)
            if not misc.is_production():
                LOGGER.info('Not production, skipping work.')
                return True
            head_pipeline_id = msg.payload['object_attributes']['head_pipeline_id']
            gl_project = gl_instance.projects.get(namespace)
            gl_mr = gl_project.mergerequests.get(mr_id)
            common.cancel_pipeline(gl_project, head_pipeline_id)
            new_pipeline_id = common.create_mr_pipeline(gl_mr)
            new_pipeline_url = f'{gl_project.web_url}/-/pipelines/{new_pipeline_id}'
            note_text = ("This MR's current target branch has changed since the last pipeline"
                         f" was triggered. The last pipeline {head_pipeline_id} has been canceled"
                         f" and a new pipeline has been triggered: {new_pipeline_url}  \n"
                         f"Last pipeline MR target branch: {ds_branch}  \n"
                         f"Current MR target branch: {mr_branch}  ")
            common.create_note(gl_mr, note_text)
            return True
    LOGGER.info("MR current target branch '%s' matches latest pipeline.", mr_branch)
    return False


def process_mr_event(msg, **_):
    """Process an MR message."""
    mr_id = msg.payload['object_attributes']['iid']
    mr_labels = [label['title'] for label in msg.payload['labels']]
    namespace = msg.payload['project']['path_with_namespace']
    event_user = msg.payload['user']['username']
    LOGGER.info('Processing MR event for %s!%s from %s.', namespace, mr_id, event_user)

    if process_possible_branch_change(event_user, namespace, mr_id, msg):
        return

    # If someone changed the CKI labels then recalculate them.
    if cki_label_changed(msg.payload['changes']):
        LOGGER.info('CKI labels changed. Calculating new ones.')
        if labels_to_add := compute_new_labels(event_user, namespace, mr_id):
            # Don't replace the Waived scope.
            if f'{defs.CKI_KERNEL_RT_PREFIX}::Waived' in mr_labels:
                LOGGER.info('MR has %s::Waived, discarding new RT label.',
                            defs.CKI_KERNEL_RT_PREFIX)
                labels_to_add = [lbl for lbl in labels_to_add if not
                                 lbl.startswith(f'{defs.CKI_KERNEL_RT_PREFIX}::')]
            add_labels(namespace, mr_id, labels_to_add)
        return
    # or if the MR doesn't have CKI labels add ::Missing ones.
    if labels_to_add := check_for_missing_labels(mr_labels):
        LOGGER.info('MR is missing one or more CKI labels.')
        add_labels(namespace, mr_id, labels_to_add)
        return
    LOGGER.info('No CKI label changes to make.')
    return


def process_note_event(msg, **_):
    """Process a note message."""
    if not common.force_webhook_evaluation(msg.payload['object_attributes']['note'], 'cki'):
        LOGGER.info('Note event did not request evaluation, ignoring.')
        return
    mr_id = msg.payload['merge_request']['iid']
    namespace = msg.payload['project']['path_with_namespace']
    event_user = msg.payload['user']['username']
    LOGGER.info('Processing note event for %s!%s from %s.', namespace, mr_id, event_user)
    if labels_to_add := compute_new_labels(event_user, namespace, mr_id):
        add_labels(namespace, mr_id, labels_to_add)
        return
    LOGGER.info('No CKI label changes to make.')
    return


def process_pipeline_event(msg, **_):
    """Process a pipeline message."""
    # This should filter out upstream pipeline events and any downstream pipeline events which are
    # not directly related to an MR.
    if not (mr_url := common.get_pipeline_variable(msg.payload, 'mr_url')) or \
       ((retrigger := common.get_pipeline_variable(msg.payload, 'retrigger')) and
           misc.strtobool(retrigger)):
        LOGGER.info('Event did not contain the expected variables, ignoring')
        return

    namespace, mr_id = parse_mr_url(mr_url)
    event_user = msg.payload['user']['username']
    pipeline_name = common.get_pipeline_variable(msg.payload, 'trigger_job_name')
    LOGGER.info('Processing pipeline event (id: %s, name: %s) for %s!%s.',
                msg.payload['object_attributes']['id'], pipeline_name, namespace, mr_id)

    pipeline_data = FetchCKI(event_user, namespace, mr_id).run_query()
    pipeline = next((pipe for pipe in map_pipeline_query_results(pipeline_data)
                     if pipe.name == pipeline_name), None)
    if pipeline and pipeline.label:
        LOGGER.info('Setting label %s.', pipeline.label)
        add_labels(namespace, mr_id, [pipeline.label])
    else:
        LOGGER.info('No label to set for this pipeline.')
    return


WEBHOOKS = {
    'merge_request': process_mr_event,
    'note': process_note_event,
    'pipeline': process_pipeline_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, get_gl_instance=False)


if __name__ == "__main__":
    main(sys.argv[1:])
