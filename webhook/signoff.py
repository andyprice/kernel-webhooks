"""Ensure MR commits have the necessary DCO signoff."""
import difflib
import enum
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from unidecode import unidecode

from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.signoff')
SESSION = session.get_session('cki.webhook.signoff')


class State(enum.IntEnum):
    """Possible commit DCO check results."""

    OK = 0
    MISSING = 1
    EMAILMISMATCH = 2
    NAMEMISMATCH = 3
    BADAUTHOR = 4
    BADFORMAT = 5


footnotes = {
    # State.OK: "A valid DCO Signoff was found for this commit.",

    State.MISSING: "No Valid DCO Signoff was found.",
    State.EMAILMISMATCH: "Signed-off-by email does not match.",
    State.NAMEMISMATCH: "Signed-off-by name does not match.",
    State.BADAUTHOR: "Commit email not in redhat.com, must be rewritten.",
    State.BADFORMAT: "Signed-off-by line is incorrectly formatted."
}


def get_current_signoff_scope(labels):
    """Get current Signoff label scope."""
    for label in labels:
        if label.startswith("Signoff::"):
            return label.split(":")[2]
    return None


def find_dco(gl_instance, text, name, username, email):
    """Look for DCO string and return state."""
    # pylint: disable=too-many-branches,too-many-locals,too-many-statements
    state = State.MISSING
    expected_dco = f"Signed-off-by: {name} <{email}>"
    found_name = ''
    found_email = ''
    matched_sob = ''
    delta = ''
    markdown_pattern = r'^Signed-off-by: (?P<name>[a-zA-Z ]*) '
    markdown_pattern += r'(?P<mkd>\[(?P<e1>[a-z]*@[a-zA-Z0-9.]*)\]'
    markdown_pattern += r'\(mailto:(?P<e2>[a-z]*@[a-zA-Z0-9.]*)\))'
    markdown_re = re.compile(markdown_pattern, re.IGNORECASE)

    for line in text.splitlines():
        if unidecode(line.rstrip()) == unidecode(expected_dco):
            state = State.OK
            delta = ''
            break
        if line.startswith("Signed-off-by:"):
            name_email_angles = line.rstrip().removeprefix('Signed-off-by: ')
            found_name_email = name_email_angles.removesuffix('>').split(' <')
            found_name = found_name_email[0]
            markdown = markdown_re.match(unidecode(line.rstrip()))
            if len(found_name_email) == 2:
                found_email = found_name_email[1]
            elif markdown and markdown.group('e1') == markdown.group('e2'):
                state = State.BADFORMAT
                delta = 'Unexpected markdown found: `' + markdown.group('mkd') + '`'
                continue
            elif '<' not in line or '>' not in line:
                delta = f'Missing angle bracket(s) in `{line}`'
                state = State.BADFORMAT
                continue
            state = State.MISSING
            if found_email != email:
                if common.match_gl_username_to_email(gl_instance, found_email, username):
                    LOGGER.info("found_email %s maps to username %s", found_email, username)
                    state = State.OK
                    delta = ''
                    break
                if "redhat.com" not in email:
                    state = State.BADAUTHOR
                    delta = f'Commit author `<{email}>` not in redhat.com'
                else:
                    state = State.EMAILMISMATCH
                    delta = f'Expected: `<{email}>`, Found: `<{found_email}>`'
            elif found_name != name:
                state = State.NAMEMISMATCH
                delta = f'Expected: `{name}`, Found: `{found_name}`'
            matched_sob = line.rstrip()

    if state in (State.EMAILMISMATCH, State.NAMEMISMATCH):
        LOGGER.info("DCO mismatch:")
        diff = difflib.unified_diff(expected_dco.split('\n'), matched_sob.split('\n'), lineterm="")
        for line in diff:
            if "Signed-off-by:" in line:
                LOGGER.info("%s", line)

    LOGGER.debug("Expected DCO: '%s' - %s", expected_dco, state.name)
    return state, expected_dco.removeprefix('Signed-off-by: '), delta


def process_commits(instance, project, mr_commits):
    """For a given set of commits return a dict of {commit.id: State}."""
    commits = {}
    for commit in mr_commits:
        merge_title_prefixes = ["Merge: ", "Merge branch "]
        if any(commit.title.startswith(prefix) for prefix in merge_title_prefixes):
            commit = project.commits.get(commit.id)
        # Skip merge commits
        if len(commit.parent_ids) > 1:
            LOGGER.debug("Merge commit? Skipping.")
            continue
        commits[commit.id] = find_dco(instance, commit.message, commit.author_name,
                                      None, commit.author_email)
    return commits


def generate_table(commits):
    """Generate a markdown table of results."""
    # header
    results_table = "**DCO Signoff "
    if all(state[0] == State.OK for state in commits.values()):
        results_table += "Report**\n\n"
        results_table += defs.DCO_PASS
        if misc.is_production():
            return results_table
    else:
        results_table += "Error(s)!**\n\n"
        results_table += defs.DCO_FAIL
    # table of results
    header = "| **Commit** | **Signoff Status** | **Delta** |\n|----|----|----|\n"
    table = ""
    for commit, result in commits.items():
        state = result[0]
        delta = result[2]
        if state is State.OK:
            continue
        table += f"| {commit} | **{state.name}** [^{state.value}] | {delta} |\n"
    # footer (footnotes)
    footer = ""
    for state, text in footnotes.items():
        footer += f"[^{state.value}]: {text}\n"
    results_table += common.wrap_comment_table(header, table, footer, "table")
    return results_table


def update_mr(gl_instance, gl_project, merge_request, results_table):
    """Update the MR with a note of the results and possibly set the label scope."""
    current_scope = get_current_signoff_scope(merge_request.labels)
    if defs.DCO_PASS in results_table:
        new_scope = defs.READY_SUFFIX
    else:
        new_scope = defs.NEEDS_REVIEW_SUFFIX
    LOGGER.debug("current scope: '%s', new scope: '%s'", current_scope, new_scope)

    common.add_label_to_merge_request(gl_instance, gl_project, merge_request.iid,
                                      [f'Signoff::{new_scope}'])
    common.update_webhook_comment(merge_request, gl_instance.user.username, "**DCO Signoff ",
                                  results_table)


def get_head_commit_author(gl_mergerequest):
    """Fetch the commit author_email for the HEAD commit of the MR."""
    for commit in gl_mergerequest.commits():
        return commit.author_email
    return "MR has no pending commits to ascertain author from"


def _do_process_mr(gl_instance, message, mr_id):
    gl_project = gl_instance.projects.get(message.payload["project"]["path_with_namespace"])
    if not (gl_mergerequest := common.get_mr(gl_project, mr_id)):
        return

    dco_results = process_commits(gl_instance, gl_project, gl_mergerequest.commits())

    # Check the MR description too.
    mr_name = gl_mergerequest.author['name']
    mr_username = gl_mergerequest.author['username']
    head_commit_email = get_head_commit_author(gl_mergerequest)
    dco_results['MR Description'] = find_dco(gl_instance, gl_mergerequest.description,
                                             mr_name, mr_username, head_commit_email)

    results_table = generate_table(dco_results)
    update_mr(gl_instance, gl_project, gl_mergerequest, results_table)


def process_mr(gl_instance, message, **_):
    """Process a merge request message."""
    desc_changed = 'description' in message.payload.get('changes', {})
    label_changed = common.has_label_prefix_changed(message.payload['changes'], 'Signoff::')
    if not common.mr_action_affects_commits(message) and not desc_changed and not label_changed:
        return

    _do_process_mr(gl_instance, message, message.payload["object_attributes"]["iid"])


def process_note(gl_instance, message, **_):
    """Process a merge request only if request-signoff-evluation was specified."""
    if not common.force_webhook_evaluation(message.payload['object_attributes']['note'], 'signoff'):
        return

    _do_process_mr(gl_instance, message, message.payload["merge_request"]["iid"])


WEBHOOKS = {
    "merge_request": process_mr,
    "note": process_note,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SIGNOFF')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
