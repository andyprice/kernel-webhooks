"""GraphQL Fragments to use with queries.

See https://graphql.org/learn/queries/#fragments

"""

CKI_PIPELINE = """
fragment CkiPipeline on Pipeline {
  id
  project {
    id
  }
  sourceJob {
    name
  }
  status
  stages {
    nodes {
      name
      jobs {
        nodes {
          status
        }
      }
    }
  }
}
"""

CURRENT_USER = """
fragment CurrentUser on Query {
  currentUser {
    id
    name
    username
  }
}
"""

MR_COMMITS = """
fragment MrCommits on MergeRequest {
  commits: commitsWithoutMergeCommits(after: $cursor) {
    pageInfo {
      hasNextPage
      endCursor
    }
    nodes {
      sha
      description
    }
  }
}
"""

MR_FILES = """
fragment MrFiles on MergeRequest {
  files: diffStats {
    path
  }
}
"""

MR_LABELS = """
fragment MrLabels on MergeRequest {
  labels {
    nodes {
      title
    }
  }
}
"""
