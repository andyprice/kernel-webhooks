"""Common variable definitions that can be used by all webhooks (and common code)."""
from enum import IntEnum
from enum import auto

EMAIL_BRIDGE_ACCOUNT = 'redhat-patchlab'
BOT_ACCOUNTS = ('cki-bot', 'cki-kwf-bot', EMAIL_BRIDGE_ACCOUNT)
KERNEL_BZ_BOT = 'cki-ci-bot+kernel-workflow-bugzilla@redhat.com'
ARK_PROJECT_ID = 13604247

MAX_COMMITS_PER_MR = 2000
TABLE_ENTRY_THRESHOLD = 5

UMB_BRIDGE_MESSAGE_TYPE = 'cki.kwf.umb-bz-event'

LABELS_YAML_PATH = 'utils/labels.yaml'
RH_METADATA_YAML_PATH = 'utils/rh_metadata.yaml'

CONFIG_LABEL = 'Configuration'
NEEDS_REVIEW_SUFFIX = 'NeedsReview'
NEEDS_TESTING_SUFFIX = 'NeedsTesting'
MISSING_SUFFIX = 'Missing'
TESTING_FAILED_SUFFIX = 'TestingFailed'
TESTING_WAIVED_SUFFIX = 'Waived'
READY_SUFFIX = 'OK'
BLOCKED_BY_PREFIX = 'Blocked-by:'
BLOCKED_SUFFIX = 'Blocked'
TESTING_SUFFIXES = (NEEDS_TESTING_SUFFIX, TESTING_FAILED_SUFFIX)

BASE_DEPENDENCIES = [f'Acks::{READY_SUFFIX}',
                     f'CKI::{READY_SUFFIX}',
                     f'CommitRefs::{READY_SUFFIX}',
                     f'Signoff::{READY_SUFFIX}']

READY_FOR_MERGE_DEPS = BASE_DEPENDENCIES + [f'Bugzilla::{READY_SUFFIX}',
                                            f'Dependencies::{READY_SUFFIX}']
READY_FOR_QA_DEPS = BASE_DEPENDENCIES + [f'Bugzilla::{NEEDS_TESTING_SUFFIX}']

READY_FOR_MERGE_LABEL = 'readyForMerge'
READY_FOR_QA_LABEL = 'readyForQA'
TARGETED_TESTING_LABEL = 'TargetedTestingMissing'
BZ_FAILED_QA = 'FailedQA'
BZ_PASSED_QA = 'Tested'
BZ_STATE_NEW = 'NEW'
BZ_STATE_ASSIGNED = 'ASSIGNED'
BZ_STATE_POST = 'POST'
BZ_STATE_MODIFIED = 'MODIFIED'
BZ_STATE_CLOSED = 'CLOSED'
BZ_NUMBER_INVALID = 'INVALID'
BUG_FIELDS = ['cf_internal_target_release',
              'cf_verified',
              'cf_zstream_target_release',
              'component',
              'external_bugs',
              'flags',
              'id',
              'product',
              'status',
              'summary'
              ]

EXT_TYPE_URL = 'https://gitlab.com/'
BZ_IN_DESCRIPTION_ONLY = 'DescOnly'
CODE_CHANGED_PREFIX = "CodeChanged::"
CKI_KERNEL_PREFIX = 'CKI'
CKI_KERNEL_RT_PREFIX = 'CKI_RT'
DCO_URL = "https://developercertificate.org"
DCO_PASS = "The DCO Signoff Check for all commits and the MR description has **PASSED**.\n"
DCO_FAIL = ("**ERROR: DCO 'Signed-off-by:' tags were not found on all commits and the MR "
            "description. Please review the results in the table below.**  \n"
            "This project requires developers add a Merge Request description and per-commit "
            f"acknowlegement of the [Developer Certificate of Origin]({DCO_URL}), also known "
            "as the DCO. This can be accomplished by adding an explicit 'Signed-off-by:' tag "
            "to your MR description and each commit.\n\n"
            "**This Merge Request's commits will not be considered for inclusion into this "
            "project until these problems are resolved. After making the required changes please "
            "resubmit your merge request for review.**\n\n")
SUBSYS_LABEL_PREFIX = 'Subsystem'
NOTIFICATION_HEADER = "Notifying users:"
NOTIFICATION_TEMPLATE = ("{header} {users}  \nThis is the Subsystems hook's user notification"
                         " system for file changes. Please see the"
                         " [kernel-watch project]({project}) for details.")

INTERNAL_FILES = ('redhat/', '.gitlab', '.get_maintainer.conf', 'makefile')


class BZPriority(IntEnum):
    """Possible priority of a bugzilla BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#priority
    UNKNOWN = auto()
    UNSPECIFIED = auto()
    LOW = auto()
    MEDIUM = auto()
    HIGH = auto()
    URGENT = auto()

    @classmethod
    def from_str(cls, priority_str):
        """Return the BZPriority matching the string, or BZPriority.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == priority_str.upper()), cls.UNKNOWN)


class BZStatus(IntEnum):
    """Possible status of a bugzilla BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#bug_status
    UNKNOWN = auto()
    NEW = auto()
    ASSIGNED = auto()
    POST = auto()
    MODIFIED = auto()
    ON_DEV = auto()
    ON_QA = auto()
    VERIFIED = auto()
    RELEASE_PENDING = auto()
    CLOSED = auto()

    @classmethod
    def from_str(cls, status):
        """Return the BZStatus matching the status string, or BZStatus.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == status.upper()), cls.UNKNOWN)


class BZResolution(IntEnum):
    """Possible resolution of a BZStatus.CLOSED BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#resolution
    UNKNOWN = auto()
    CURRENTRELEASE = auto()
    DUPLICATE = auto()
    ERRATA = auto()
    NOTABUG = auto()
    WONTFIX = auto()
    CANTFIX = auto()
    DEFERRED = auto()
    INSUFFICIENT_DATA = auto()
    NEXTRELEASE = auto()
    RAWHIDE = auto()
    UPSTREAM = auto()
    WORKSFORME = auto()
    EOL = auto()

    @classmethod
    def from_str(cls, resolution):
        """Return the BZResolution matching the status string, or BZResolution.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == resolution.upper()), cls.UNKNOWN)


class MrScope(IntEnum):
    """Possible scopes of an MR."""

    INVALID = auto()
    CLOSED = auto()
    NEEDS_REVIEW = auto()
    READY_FOR_QA = auto()
    READY_FOR_MERGE = auto()
    MERGED = auto()

    def label(self, prefix):
        """Return a formatted label string."""
        match self.name:
            case 'NEEDS_REVIEW':
                scope = NEEDS_REVIEW_SUFFIX
            case 'READY_FOR_QA':
                scope = NEEDS_TESTING_SUFFIX
            case 'READY_FOR_MERGE':
                scope = READY_SUFFIX
            case _:
                scope = self.name.capitalize()
        return f'{prefix}::{scope}'


class MrState(IntEnum):
    """Possible states of a GL MR."""

    # https://docs.gitlab.com/ee/api/graphql/reference/#mergerequeststate
    UNKNOWN = auto()
    ALL = auto()
    CLOSED = auto()
    LOCKED = auto()
    MERGED = auto()
    OPENED = auto()

    @classmethod
    def from_str(cls, state):
        """Return the MrState matching the state string, on MrState.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == state.upper()), cls.UNKNOWN)
