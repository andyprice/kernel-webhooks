#!/usr/bin/env python
"""Nudge webhooks for active MRs."""

import argparse
import importlib
import os
import sys

from cki_lib.gitlab import get_instance
from gitlab.exceptions import GitlabGetError


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Nudge webhooks for active MRs')
    parser.add_argument('--project-ids', default=os.environ.get('PROJECT_IDS', '').split(),
                        help='List of Gitlab project IDs to nudge.', nargs='+')
    parser.add_argument('--nudge-action', default=os.environ.get('NUDGE_ACTION'),
                        help=("Action to take to nudge the MR. Can be one of:"
                              " note, label, trigger."))
    parser.add_argument('--note-text', default=os.environ.get('NOTE_TEXT'),
                        help="If action is 'note', then this is the text to post.")
    parser.add_argument('--search-label', default=os.environ.get('SEARCH_LABEL'),
                        help=("Operate on open MRs with the given label."
                              " Also the label to remove for the 'label' nudge action."))
    parser.add_argument('--trigger-hook', default=os.environ.get('TRIGGER_HOOK', 'bugzilla'),
                        help="Hook to trigger for nudge action 'trigger'. Defaults to bugzilla.")
    parser.add_argument('--trigger-args', help=("Extra arguments to pass to the trigger hook."
                                                " --merge-request is provided automatically."))
    parser.add_argument('--dry-run', action='store_true',
                        help=("If dry-run is True, print the action we would take,"
                              " don't actually do it."))
    return parser.parse_args()


def run_trigger_hook(mr, hook, extra_args, dry_run):
    """Run the given hook."""
    args = ['--merge-request', mr.web_url]
    if extra_args:
        args = args + extra_args.split()
    print('Calling %s with: %s' % (hook.__name__, args))
    if not dry_run:
        hook.main(args)


def _do_nudges(mr_list, trigger_module, args):
    """Process the list of MRs."""
    if not mr_list:
        print('No MRs to nudge.')
        return

    print(f'{len(mr_list)} MRs to nudge.')
    for mr in mr_list:
        # Handle trigger action.
        if args.nudge_action == 'trigger':
            run_trigger_hook(mr, trigger_module, args.trigger_args, args.dry_run)
            continue

        # Set up and run label or note action.
        if args.nudge_action == 'label':
            note_body = f'/unlabel "{args.label}"'
        elif args.nudge_action == 'note':
            note_body = args.note_text

        print(f"For MR {mr.iid} leaving note: {note_body}")
        if not args.dry_run:
            mr.notes.create({'body': '{note_body}'})


def get_merge_requests(project, label):
    """Return a merge request list from the given project. Can search by label."""
    if label:
        return project.mergerequests.list(iterator=True, state='opened', labels=label)
    return project.mergerequests.list(iterator=True, state='opened')


def main():
    """Run main loop."""
    args = _get_parser_args()
    trigger_module = None
    print(f'dry run: {args.dry_run}')

    if not args.project_ids:
        print("ERROR: Missing required --project-ids (PROJECT_IDS) parameter.")
        sys.exit(1)

    if args.nudge_action == 'trigger':
        try:
            trigger_module = importlib.import_module(f'webhook.{args.trigger_hook}')
        except ModuleNotFoundError:
            print(f"Cannot find trigger hook module '{args.trigger_hook}'.")
            sys.exit(1)

    if args.nudge_action not in ('label', 'note', 'trigger'):
        print("ERROR: nudge action must be one of: label, note, trigger.")
        sys.exit(1)

    if args.nudge_action == 'label' and not args.search_label:
        print("ERROR: 'label' nudge action requires --search-label (SEARCH_LABEL).")
        sys.exit(1)

    if args.nudge_action == 'note' and not args.note_text:
        print("ERROR: 'note' nudge action requires --note-text (NOTE_TEXT).")
        sys.exit(1)

    gl = get_instance('https://gitlab.com')

    for project_id in args.project_ids:
        try:
            gl_project = gl.projects.get(project_id)
        except GitlabGetError:
            print(f'Error fetching project with ID {project_id}.')
            continue
        print(f'Processing MRs on project: {gl_project.name} ({gl_project.id})')

        mr_list = get_merge_requests(gl_project, args.search_label)
        _do_nudges(mr_list, trigger_module, args)


if __name__ == '__main__':
    main()
