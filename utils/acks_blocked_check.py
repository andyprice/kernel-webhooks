#!/usr/bin/env python
"""Weed out MRs that have no blocking comments but an Acks::Blocked label."""

import argparse
from copy import deepcopy
from importlib import import_module
import os
import sys

from cki_lib import logger
from gql import gql

from webhook import common

LOGGER = logger.get_logger('utils.acks_blocked_check')

# Get the resolve details for every discussion of the given MR.
MR_QUERY = """
query mrData($namespace: ID!, $mr_id: String!, $cursor: String!) {
  project(fullPath: $namespace) {
    mergeRequest(iid: $mr_id) {
      discussions(after: $cursor) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          resolvable
          resolved
        }
      }
    }
  }
}
"""

# Get all open MRs with an Acks::Blocked label and their discussion resolve details.
BLOCKED_QUERY = """
query mrData($namespace: ID!, $cursor: String!) {
  %s(fullPath: $namespace) {
    mergeRequests(state: opened, labels: "Acks::Blocked", after: $cursor) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        webUrl
        project {
          fullPath
        }
        discussions {
          pageInfo {
            hasNextPage
            endCursor
          }
          nodes {
            resolvable
            resolved
          }
        }
      }
    }
  }
}
"""


def do_query(client, query, values, expected_key):
    """Do a graphql query."""
    result = client.execute(query, variable_values=values)
    if not result[expected_key]:
        LOGGER.error('Gitlab did not return the expected data: %s', result)
        sys.exit(1)
    return result


def mr_has_blocked_discussions(client, mr):
    """Return True if the MR has resolveable discussions which are not resolved."""
    if not mr['discussions']['nodes']:
        return False

    mr_is_blocked = False
    mr = deepcopy(mr)
    if mr['discussions']['pageInfo']['hasNextPage']:
        query_params = {'namespace': mr['project']['fullPath'],
                        'mr_id': mr['iid'],
                        'cursor': mr['discussions']['pageInfo']['endCursor']
                        }
        while query_params['cursor'] is not None:
            result = do_query(client, gql(MR_QUERY), query_params, 'project')
            mr_discussions = result['project'].get('mergeRequest', {}).get('discussions')
            query_params['cursor'] = mr_discussions['pageInfo']['endCursor'] if \
                mr_discussions['pageInfo']['hasNextPage'] else None
            mr['discussions']['nodes'].extend(mr_discussions['nodes'])

    for discussion in mr['discussions']['nodes']:
        if discussion['resolvable'] and not discussion['resolved']:
            mr_is_blocked = True
            break

    LOGGER.debug('MR %s: %s discussions, unresolved: %s', mr['iid'],
                 len(mr['discussions']['nodes']), mr_is_blocked)
    return mr_is_blocked


def get_blocked_mrs(client, namespace, namespace_type):
    """Return a list of the MR IDs with the Acks::Blocked label & no blocked discussions."""
    mr_list = []
    cursor = ''
    count = 0
    while cursor is not None:
        query_params = {'namespace': namespace,
                        'cursor': cursor,
                        'nstype': namespace_type
                        }
        result = do_query(client, gql(BLOCKED_QUERY % namespace_type), query_params, namespace_type)

        mr_data = result[namespace_type].get('mergeRequests')
        cursor = mr_data['pageInfo']['endCursor'] if mr_data['pageInfo']['hasNextPage'] else None
        count += len(mr_data['nodes'])

        for mr in mr_data['nodes']:
            if not mr_has_blocked_discussions(client, mr):
                mr_list.append(mr['webUrl'])

    LOGGER.info('%s %s MRs: found %s (%s not blocked)', namespace_type.title(), namespace, count,
                len(mr_list))
    return mr_list


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Confirm Acks::Blocked label')

    # Global options
    parser.add_argument('-g', '--groups', default=os.environ.get('GL_GROUPS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    return parser.parse_args()


def main():
    """Find Acks::Blocked MRs and check for blocking comments.."""
    args = _get_parser_args()
    LOGGER.info('Finding blocked MRs...')

    client = common.gl_graphql_client()
    mrs_with_resolved_discussions = []
    for group in args.groups:
        mrs_with_resolved_discussions.extend(get_blocked_mrs(client, group, 'group'))
    for project in args.projects:
        mrs_with_resolved_discussions.extend(get_blocked_mrs(client, project, 'project'))

    if not mrs_with_resolved_discussions:
        LOGGER.info('All MRs with Acks::Blocked label have a blocked discussion, nothing to do.')
        return

    ack_nack = import_module('webhook.ack_nack')
    for mr in mrs_with_resolved_discussions:
        LOGGER.info('Triggering ack_nack for %s', mr)
        args = ['--merge-request', mr]
        ack_nack.main(args)


if __name__ == '__main__':
    main()
